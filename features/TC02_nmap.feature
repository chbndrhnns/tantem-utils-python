# Created by sbulic at 15.02.19.
@TC02_nmap
Feature: TC02 nmap
#User story:
#As a PFP user
#I want to port scan test case
#so that I check open ports on VMs from post deployment artifact

  Scenario: TC02 nmap
    Given postdeployment file is available
    When I scan ports on VMs
    Then I get response
