# tantem-utils-python

[![coverage report](https://gitlab.tools.in.pan-net.eu/TAAS/tantem-utils-python/badges/master/coverage.svg)](https://gitlab.tools.in.pan-net.eu/TAAS/tantem-utils-python/commits/master)
[![pipeline status](https://gitlab.tools.in.pan-net.eu/TAAS/tantem-utils-python/badges/master/pipeline.svg)](https://gitlab.tools.in.pan-net.eu/TAAS/tantem-utils-python/commits/master)

This project contains utilities for the interaction with Tantem.

## Utitily details

### `TestIdsExtractor` component

This component reads the value of the environment variable where Tantem sends the test cases and writes them to a 
temporary file where behave can pick them up. This is to circumvent an assumed 
[bug](https://github.com/behave/behave/issues/779) in behave.

<details><summary>Click to toggle</summary>

```bash
pipenv run python -m tantem_utils.extractor
```  

</details>


### `notifier` component

This component can be used to send updates to Tantem during a test execution from Python-based projects.

For [`behave`](https://behave.readthedocs.io/en/latest/), modify your `environment.py` file to include these lines:

<details><summary>Click to toggle</summary>

```python
from tantem_utils import Notifier

def before_all(context):
    context.notifier: Notifier = Notifier()

def before_scenario(context):
    context.notifier.test_case_started(context.scenario.name)

def after_scenario(context):
    context.notifier.test_case_stopped(context.scenario.name)
```  

</details>

### `report_uploader` component

This component can be used to send a test execution report to Tantem after the last test case has been executed.

It [cannot be used](https://github.com/behave/behave/issues/639#issuecomment-379163225) in the `after_all()` hook 
although this seems like the best place to call it. Instead, we are running it after behave is finished as part of the CI
pipeline.

### `testcase_updater` component

This component is supposed to be run from the CI pipeline after a feature file changes.
It generates a list of test cases and is supposed to run when a feature file changes.

An [architectural overview](http://e174358.docs.tools.in.pan-net.eu/notebook/memos/19-02-22-tantem-integration.html#uc1-tantem-is-notified-when-tests-are-modified) is also available.

When invoked, the updater

- looks for files with the extension `.feature` in the folder `features/`,
- extract the names of the test cases (scenarios),
- create a JSON structure according to the scheme presented below,
- sends the data to the specified Tantem instance.

The tool has been tested on features files for the following frameworks:

- [x] `behave`

## Configuration

The configuration happens via the following environment variables (sorted alphabetically):

| Variable                  | Required | Value                                                            | Default |
| ------------------------- | :------: | ---------------------------------------------------------------- | ------- |
| CI_PROJECT_ID             |    X     | Gitlab project ID                                                | None    |
| CI_PIPELINE_ID            |    X     | Gitlab pipeline ID                                               | None    |
| CI_PROJECT_URL            |    X     | (for integration tests) GitLab project URL                       | None    |
| LOG_LEVEL                 |          | ERROR, WARNING, INFO, DEBUG                                      | `INFO`  |
| REPORT_LOCATION           |    X     | Location of the report that is uploaded to Tantem                | artifacts/service-tests-json/results.json    |
| TANTEM_AUTH_USER          |    X     | User for authentication                                          | None    |
| TANTEM_AUTH_PASSWORD      |    X     | Password for authentication                                      | None    |
| TANTEM_BASE_URL           |    X     | Protocol and host of Tantem                                      | None    |
| TESTS_LOCATION            |    X     | Location of the feature files                                    | None    |

The variables `TANTEM_BASE_URL`, `TANTEM_AUTH_PASSWORD`, and `TANTEM_AUTH_USER` are usually set on the GitLab group level.

## Getting started

- Run `pipenv install -e git+https://gitlab.tools.in.pan-net.eu/TAAS/tantem-utils-python.git#egg=tantem-utils`
- Run the test case updater tool:

  ```bash
  CI_PROJECT_ID=1234 \
  TANTEM_BASE_URL=http://tantem-dev.ctf.in.pan-net.eu \
  TANTEM_AUTH_USER=gitlab-user \
  TANTEM_AUTH_PASSWORD=abc \
  TESTS_LOCATION=./features \
  pipenv run python3 main.py`
  ```

- If you need to build the Docker image, run `make build`
- If you need to run the Docker image, run `make run` and specifiy the required parameters on the command line

## Example CI job

If you want to make use of the `testcase_updater` tool in your project, follow these steps to integrate it:

### 1. Add a job to your CI configuration

A [template](https://gitlab.tools.in.pan-net.eu/TAAS/_shared/.gitlab-ci/tantem-testcase-updater.yml) for this job is part of this repository.

It can easily be included by adding these lines to your `.gitlab-ci.yml` file:

```yaml
include:
 - project: 'TAAS/_shared'
   ref: master
   file: '.gitlab-ci/tantem-testcase-updater.yml'
```

### 2. Set the necessary configuration options

The required parameters are described above. Make sure the job has access to these settings. For the TAAS GitLab group,
the settings are applied on group level via CI variables.


## Testing

Unit tests can be run with `pytest tests/unit`.

Integration tests can be run with `pytest -k integration`. 

End-to-end tests can be run with `pytest -k e2e`. A 
[testing project](https://gitlab.tools.in.pan-net.eu/TAAS/testing/tantem-integration-tests) exists for these kinds of 
tests.

To create the necessary environment variables, fill in and run this snippet:

```bash
 export CI_PROJECT_ID=3416
 export CI_PIPELINE_ID=54321
 export TANTEM_AUTH_USER=gitlab-user@pan-net.eu
 export TANTEM_AUTH_PASSWORD=correct-password
 export TANTEM_BASE_URL=https://tantem-dev.ctf.in.pan-net.eu
```

## Todo

- [ ] [Semantic versioning](https://pypi.org/project/semantic-version/)