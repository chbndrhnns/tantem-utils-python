"""Data transfer objects"""
# pylint: disable=too-few-public-methods
import abc
import json as json_lib
from typing import Any, Type, Dict, Optional, Union

from tantem_utils import exceptions as err
from tantem_utils.tantem_dtos import EventData, ReportData
from tantem_utils.utils import SnakeToCamelCaseJSONEncoder


class RequestPayload(metaclass=abc.ABCMeta):
    """HttpRequestPayload"""

    def __init__(self, data: Any,
                 encoder: Type[json_lib.JSONEncoder] = json_lib.JSONEncoder) -> None:
        self._data: Any = data or {}
        self._encoder: Type[json_lib.JSONEncoder] = encoder

    def __eq__(self, other):
        """Check for equality"""
        if not isinstance(other, RequestPayload):
            return NotImplemented  # pragma no cover
        return self._data == other._data and self._encoder == other._encoder  # pylint: disable = protected-access

    @property
    def data(self):
        """Get data"""
        return self._data

    def to_json(self) -> Union[None, str]:
        """Get data as JSON"""
        return json_lib.dumps(self._data,
                              cls=self._encoder) if self._data else None


class EmptyRequestPayload(RequestPayload):
    """Empty request payload"""

    def __init__(self, data: Any = None):
        super().__init__(data)


class DictRequestPayload(RequestPayload):
    """Payload with dictionary"""

    def __init__(self, data: dict):
        if not isinstance(data, dict):
            raise err.DataError('data must be a dictionary')
        super().__init__(
            data=data
        )


class EventDataRequestPayload(RequestPayload):
    """Payload with EventData"""

    def __init__(self, data: EventData):
        if not isinstance(data, EventData):
            raise err.DataError('data must be a EventData')
        super().__init__(
            data=data,
            encoder=SnakeToCamelCaseJSONEncoder
        )


class ResponsePayload:
    """Response object contains the response from an HTTP request"""

    def __init__(self,
                 status_code: int = None,
                 content: str = None,
                 headers: Dict[str, str] = None) -> None:
        self._content_type: Optional[str] = None
        self._json: Optional[str] = None
        self._content: Optional[str] = content
        self._headers: dict = {}
        self.status_code: int = status_code or 200
        self.headers = headers or {}

    @property
    def content(self) -> str:
        """Content of the response"""
        return self._content

    @property
    def content_type(self) -> str:
        """Get content type"""
        content_type = self.headers.get('content-type', '')
        return content_type

    @property
    def headers(self) -> Dict[str, str]:
        """Get response headers"""
        return self._headers

    @headers.setter
    def headers(self, val) -> None:
        """Set response headers

        We store all keys in lower case."""
        self._headers.update(
            dict(map(lambda kv: (kv[0].lower(), kv[1]), val.items())))

    @property
    def json(self) -> Optional[dict]:
        """JSON-decoded response content"""
        data: dict = {}
        if self._content:
            try:
                data = json_lib.loads(self._content)
            except (json_lib.JSONDecodeError, ValueError, TypeError):
                raise err.DataError(f'Cannot decode to json: {err}')

        return data

    def raise_for_status(self) -> None:
        """Check if an  error occured during a request"""
        if self.status_code > 300:
            raise err.RequestError(f'Received status code: {self.status_code}')


class ReportPayload(RequestPayload):
    """Report payload"""

    def __init__(self, data: ReportData):
        if not isinstance(data, ReportData):
            raise err.DataError('data must be a ReportData')
        super().__init__(
            data=data,
            encoder=SnakeToCamelCaseJSONEncoder
        )
