"""Notifier data transfer objects"""
import abc

from tantem_utils.tantem_dtos import EventData


class Event(metaclass=abc.ABCMeta):
    """Event interface"""
    __test__ = False

    def __init__(self, data: EventData) -> None:
        self._data: EventData = data

    def __eq__(self, other):
        if not isinstance(other, Event):
            return NotImplemented
        return self._data == other._data  # pylint: disable=protected-access

    @property
    def data(self) -> EventData:
        """Get event data"""
        return self._data

    def set_event_details(self, **kwargs) -> None:
        """
        Set event details

        Can be used to modify event data that is not available at creation time
        """
        for prop, val in kwargs.items():
            setattr(self._data, prop, val)


class TestCaseStartedEvent(Event):
    """TestCaseStartedEvent"""


class TestCaseStoppedEvent(Event):
    """TestCaseStoppedEvent"""
