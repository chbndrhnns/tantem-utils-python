"""Notifier class"""
import logging
from typing import Type

from tantem_utils import exceptions as err, utils
from tantem_utils.dtos import EventDataRequestPayload
from tantem_utils.notifier.dtos import Event, TestCaseStartedEvent
from tantem_utils.tantem_dtos import EventData
from tantem_utils.utility import Utility

logger = logging.getLogger(__name__)  # pylint: disable=invalid-name


class Notifier(Utility):
    """Send notifications to Tantem"""

    ENDPOINT: str = '/tim/testautomationmicrot/api/report/job'
    REQUEST_PAYLOAD_TYPE: Type[EventDataRequestPayload] = EventDataRequestPayload

    @property
    def endpoint(self) -> str:
        """Get endpoint for sending data"""
        return utils.normalize_endpoint(
            self._config.base_url,
            self.ENDPOINT
        )

    def emit(self, event: Event) -> None:
        """Emit an event"""
        if not isinstance(event, Event):
            raise err.DataError('event must be Event')

        logger.info('Sending notification: Testcase %s %s',
                    event.data.test_case_id, event.data.status)

        try:
            payload = self._create_payload(event.data)
            self._sender.send(payload)
        except err.SenderError as exc:
            raise err.NotifierError(exc)

    def test_case_started(self, test_case_id: str):
        """
        Create and send a notification for the test case started event

        This is considered a convenience method that can be called from behave
        """
        try:
            event: TestCaseStartedEvent = TestCaseStartedEvent(
                data=EventData(
                    pipeline_id=self._config.pipeline_id,
                    test_case_id=test_case_id,
                    status='started',
                    is_stopped=False
                )
            )
            self.emit(event)
        except err.TantemUtilsError as exc:
            # we do not want to disturb test execution and ignore any errors
            # here
            utils.handle_silent_error(logger, exc)

    def test_case_stopped(self, test_case_id: str, status: str = 'passed'):
        """
        Create and send a notification for the test case started event

        This is considered a convenience method that can be called from behave
        """
        try:
            event: TestCaseStartedEvent = TestCaseStartedEvent(
                data=EventData(
                    pipeline_id=self._config.pipeline_id,
                    test_case_id=test_case_id,
                    status=status,
                    is_stopped=True
                )
            )
            self.emit(event)
        except err.TantemUtilsError as exc:
            # we do not want to disturb test execution and ignore any errors
            # here
            utils.handle_silent_error(logger, exc)
