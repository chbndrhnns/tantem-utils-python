"""Extractor module"""
import configparser
import logging
import os
from typing import List

from tantem_utils.exceptions import ExtractorError

logger = logging.getLogger(__name__)  # pylint: disable=invalid-name


class TestIdsExtractor:
    """Extractor to get test ids for behave"""
    __test__ = None

    ENV_VAR: str = 'testIds'
    FILENAME: str = '.behaverc'
    SECTION_NAME: str = 'behave'
    KEY_NAME: str = 'name'

    def __init__(self):
        self.working_dir = os.getcwd()
        self._raw_data = os.getenv(self.ENV_VAR, '')

        if not self._raw_data:
            logger.info('No data found in %s. Exiting...', self.ENV_VAR)
            raise ExtractorError()

        self.config_data: configparser.ConfigParser = configparser.ConfigParser()
        self.config_data[self.SECTION_NAME] = {self.KEY_NAME: []}
        logger.info('logger realy works here -- init part')

        self._parse_and_store_input()

    @property
    def env_var(self):
        """Get variable for the test cases are stored"""
        return self.ENV_VAR

    @property
    def raw_data(self):
        """Get raw data"""
        return self._raw_data

    @property
    def testcase_ids(self) -> List:
        """Get list of test case id's"""
        return self.config_data.get(self.SECTION_NAME, 'name')

    def write_config(self, filename=None) -> None:
        """Convert configparser object to ini file"""
        filename = self.FILENAME or filename
        logger.info('Writing to: %s', filename)
        with open(os.path.join(self.working_dir, filename), 'w') as f_out:
            self.config_data.write(f_out)

    def _parse_and_store_input(self):
        """Convert data from ENV_VAR and store it in the ConfigParser object"""
        scenario_names = self.raw_data.split(",")
        scenario_names = ["^" + name.strip() + "$" for name in scenario_names]
        self.config_data[self.SECTION_NAME][self.KEY_NAME] = '\n'.join(
            scenario_names)


if __name__ == '__main__':
    try:
        TestIdsExtractor().write_config()
    except ExtractorError:
        pass
