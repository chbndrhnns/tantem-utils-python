"""Adapter for interactions with web servers"""
import json as json_lib
import logging
from typing import Optional, List, Dict, Callable, TypeVar, Any
import requests
from requests import auth as req_auth

from tantem_utils.dtos import ResponsePayload, RequestPayload, EmptyRequestPayload
from tantem_utils import exceptions as err

# This is for deep debugging the requests and responses
# import urllib.request
# import http.client
# http.client.HTTPConnection.debuglevel = 1
from tantem_utils.exceptions import RequestError

logger = logging.getLogger(__name__)  # pylint: disable=invalid-name

T = TypeVar('T', bound='Response')  # pylint: disable=invalid-name


class HttpRequestAdapter:
    """
    Adapter for handling HTTP requests
    """
    SUPPORTED_VERBS: List[str] = ['GET', 'POST', 'PUT']
    REQUEST_TIMEOUT: float = 2.0

    def __init__(self,
                 endpoint: str,
                 auth: Optional[Dict[str, str]] = None,
                 verify_ssl: bool = True) -> None:
        self._auth: Optional[req_auth.AuthBase] = None
        self._endpoint: str = endpoint
        self._headers: dict = {}
        self._ssl_verify: bool = verify_ssl
        if auth:
            self.auth = auth
        if not endpoint:
            raise err.RequestError(
                'Cannot instanciate HttpRequestAdapter without endpoint.')

    def auth(self, val: dict = None) -> None:  # pylint: disable=method-hidden
        """Set authentication for the adapter session"""

        self._auth = req_auth.HTTPBasicAuth(
            username=val.get('username'),
            password=val.get('password')
        )

    auth = property(None, auth)

    @property
    def endpoint(self) -> str:
        """Returns the endpoint for the adapter"""
        return self._endpoint

    @endpoint.setter
    def endpoint(self, val: str) -> None:
        self._endpoint = val

    @property
    def ssl_verify(self) -> bool:
        """Get the state of the SSL verification setting"""
        return self._ssl_verify

    def get(self,
            headers: Optional[Dict[str, str]] = None,
            auth: Optional[Dict[str, str]] = None) -> T:
        """Send an HTTP GET request"""
        return self._send(
            verb='get',
            headers=headers,
            auth=auth
        )

    def put(self,
            headers: Optional[Dict[str, str]] = None,
            payload: Optional[RequestPayload] = None,
            auth: Optional[Dict[str, str]] = None) -> T:
        """Sends an HTTP PUT request"""
        return self._send(
            verb='put',
            headers=headers,
            payload=payload,
            auth=auth
        )

    def post(self,
             headers: Optional[Dict[str, str]] = None,
             payload: Optional[RequestPayload] = None,
             auth: Optional[Dict[str, str]] = None) -> T:
        """Sends an HTTP POST request"""
        return self._send(
            verb='post',
            headers=headers,
            payload=payload,
            auth=auth
        )

    def _send(self,
              verb: str = 'get',
              headers: Optional[dict] = None,
              auth: Optional[req_auth.AuthBase] = None,
              payload: Optional[RequestPayload] = None) -> ResponsePayload:
        """Send a request via the HTTP requests library"""

        if verb.upper() not in self.SUPPORTED_VERBS:
            raise RequestError(f'HTTP verb {verb} not supported')
        if not self.endpoint:
            raise RequestError(f'No endpoint specified.')

        payload = payload or EmptyRequestPayload()

        if not self._is_valid_payload(payload):
            raise RequestError('payload needs to be a RequestPayload')

        try:
            self._headers.update(headers or {})
            request_action: Callable = getattr(requests, verb)
            res: requests.Response = request_action(
                auth=auth or self._auth,
                url=self.endpoint,
                headers=self._headers,
                data=payload.to_json(),
                timeout=self.REQUEST_TIMEOUT,
                verify=self.ssl_verify
            )
            res.raise_for_status()
        except requests.exceptions.RequestException as exc:
            msg: str = self._try_get_message(exc)
            raise RequestError(exc, msg)

        return ResponsePayload(
            status_code=res.status_code,
            content=res.content,
            headers=res.headers)

    @staticmethod
    def _try_get_message(exc: Exception):
        try:
            return exc.response.json()
        except (AttributeError, KeyError, json_lib.JSONDecodeError):
            pass

    @staticmethod
    def _is_valid_payload(data: Any) -> bool:
        """
        Validate the payload

        An empty payload is valid, as well.
        """
        return not data or isinstance(data, RequestPayload)
