"""Utilities for the updater"""
import json
import re
from logging import Logger

from tantem_utils.tantem_dtos import TantemData


def str2bool(val):
    """Converts a string into boolean

    Found at https://stackoverflow.com/a/715468/6112272"""

    return val.lower() in ("yes", "true", "t", "1")


def normalize_endpoint(base, path) -> str:
    """Takes a base part and path of an endpoint and normalizes it"""

    from urllib.parse import urljoin
    return urljoin(base, path)


def underscore_to_camel(name):
    """
    source: http://lopezpino.com/2015/11/12/python-dicts-naming-conventions/
    Convert a name from underscore lower case convention to camel case convention.
    Args:
        name (str): name in underscore lowercase convention.
    Returns:
        Name in camel case convention.
    """
    under_pat = re.compile(r'_([a-z])')
    return under_pat.sub(lambda x: x.group(1).upper(), name)


class SnakeToCamelCaseJSONEncoder(json.JSONEncoder):
    """SnakeToCamelCaseJSONEncoder

    It takes into account that Python uses snake_case and
    our REST API uses camelCase.
    """

    def default(self, obj: TantemData) -> dict:  # pylint: disable=arguments-differ,method-hidden
        # Known issue for pylint: https://github.com/PyCQA/pylint/issues/414
        if isinstance(obj, TantemData):
            return {
                underscore_to_camel(name): val for name,
                val in obj.__dict__.items()}
        return json.JSONEncoder.default(self, obj)  # pragma no cover


def handle_silent_error(logger: Logger, exc: Exception) -> None:
    """When running from behave, we want to continue the test
    run even if sending results to Tantem fails.

    This method handles the error
    """
    logger.warning(
        'An error occured that prevent us from sending data to Tantem: %s',
        exc)
