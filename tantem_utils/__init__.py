"""tantem_utils module"""
# pylint: disable=cyclic-import
import os
import logging
import sys

from tantem_utils import exceptions as err

# make utilities available directly from the main package
from tantem_utils.report_uploader.__main__ import ReportUploader
from tantem_utils.notifier import Notifier
from tantem_utils.testcase_updater.updater import TestcaseUpdater
from tantem_utils.config import Configuration

# Increase the version here to bump the version
__version__ = '2.0.0'


def init_logging() -> None:
    """Initialize logger"""

    format_string = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    level = os.getenv('LOG_LEVEL', logging.getLevelName(logging.DEBUG))
    logging.basicConfig(stream=sys.stdout, level=level,
                        format=format_string)


init_logging()
