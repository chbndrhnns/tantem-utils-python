"""__main__ for report uploader"""
import logging
import os
from typing import Type

from tantem_utils import exceptions as err, utils
from tantem_utils.dtos import RequestPayload, ReportPayload
from tantem_utils.exceptions import ReportUploaderError
from tantem_utils.tantem_dtos import ReportData
from tantem_utils.utility import Utility

logger = logging.getLogger(__name__)  # pylint: disable=invalid-name


class ReportUploader(Utility):
    """ReportUploader class"""

    ENDPOINT: str = '/tim/testautomationmicrot/api/report/content'
    REQUEST_PAYLOAD_TYPE: Type[RequestPayload] = ReportPayload

    def send_report(self, report_data: ReportData = None) -> None:
        """Send report to Tantem"""
        try:
            report_data: ReportData = report_data or self.compile_report_data()
            payload: RequestPayload = self._create_payload(data=report_data)
            logger.debug('ReportData: %s', report_data)
            self._sender.send(payload)
        except (err.SenderError, err.ReportUploaderError) as exc:
            raise err.ReportUploaderError(exc)

    def compile_report_data(self) -> ReportData:
        """Create a ReportData object"""
        return ReportData(
            pipeline_id=self._config.pipeline_id,
            content=''.join(self._read_report())
        )

    def _read_report(self) -> str:
        """Read report from then configured location"""
        report_location: str = self._config.report_location
        if not self._is_valid_report_location(report_location):
            raise ReportUploaderError(
                f'Cannot read report file at {report_location}')

        logger.info('Reading report from %s', report_location)

        with open(report_location) as report_file:
            return report_file.read()

    @staticmethod
    def _is_valid_report_location(report_location):
        """Check if the report exists at the given location"""
        return os.path.isfile(report_location)

    def upload(self):
        """
        Upload a report to Tantem

        This is considered a convenience method that can be called from behave
        """
        try:
            self.send_report()
        except err.TantemUtilsError as exc:
            # we do not want to disturb test execution and ignore any errors
            # here
            utils.handle_silent_error(logger, exc)


if __name__ == '__main__':
    try:
        ReportUploader().upload()
    except err.ReportUploaderError:
        # Silently pass
        pass
