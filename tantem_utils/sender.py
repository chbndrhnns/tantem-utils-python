"""Module for sending JSON data"""
import logging
import os
from typing import Optional

from tantem_utils.authenticator import Authenticator
from tantem_utils import exceptions as err
from tantem_utils import utils
from tantem_utils.config import Configuration
from tantem_utils.http import HttpRequestAdapter
from tantem_utils.dtos import \
    RequestPayload, \
    ResponsePayload, \
    EmptyRequestPayload

logger = logging.getLogger(__name__)  # pylint: disable=invalid-name


class Sender:
    """Sender class"""

    def __init__(self, endpoint: str, config: Configuration) -> None:
        self._data: Optional[RequestPayload] = None
        self._authenticator: Authenticator = Authenticator(config)
        self._http = HttpRequestAdapter(
            endpoint=endpoint,
            verify_ssl=config.verify_ssl)
        self.verify_ssl: bool = utils.str2bool(os.getenv('SSL_VERIFY', 'True'))

    @property
    def payload(self) -> RequestPayload:  # pragma: no cover
        """Getter for payload property"""
        return self._data

    @payload.setter
    def payload(self, val: dict) -> None:
        """Setter for payload property"""
        self._data = val

    def send(self, data: RequestPayload = None) -> True:
        """Sends a HTTP PUT to the configured endpoint

        payload: RequestPayload structure
        """
        payload = data or self._data or EmptyRequestPayload()
        if not isinstance(payload, RequestPayload):
            raise err.DataError('payload needs to be RequestPayload')

        token = self._get_token()

        try:
            headers: dict = {
                'Content-Type': 'application/json',
                'accept': 'application/json',
                'Authorization': f'Bearer {token}'
            }
            logger.info('Sending data...')
            logger.debug('Request headers: %s', headers)
            logger.debug('Request payload: %s', payload.to_json())
            res: ResponsePayload = self._http.put(
                headers=headers,
                payload=payload
            )
            if res.content and 'application/json' not in res.content_type:
                raise err.SenderError(
                    'We did not receive JSON data. Something went wrong.')
            logger.debug('Response: %s', res.json)
            return True
        except err.RequestError as exc:
            raise err.SenderError(f'Error response: {exc}')
        except (err.SenderError,
                ValueError) as exc:
            raise err.SenderError(f'Could not send request: {exc}')

    def _get_token(self):
        try:
            token: str = self._authenticator.token
        except err.AuthError as exc:
            raise err.SenderError(f'Could not retrieve token: {exc}')
        return token
