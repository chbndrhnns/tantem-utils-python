"""Utility base class"""
import abc
from typing import Any, Type

from tantem_utils.config import Configuration
from tantem_utils import utils
from tantem_utils.dtos import RequestPayload
from tantem_utils.sender import Sender


class Utility(metaclass=abc.ABCMeta):  # pylint: disable=too-few-public-methods
    """Base class for utilities"""

    __test__ = False

    # This allows us to use an endpoint per Utility
    ENDPOINT: str = ''

    # This constant prescribes which RequestPayload object to create
    REQUEST_PAYLOAD_TYPE: Type[RequestPayload] = RequestPayload

    def __init__(self, config: Configuration = None) -> None:
        self._config: Configuration = config or Configuration()
        self._sender: Sender = Sender(
            endpoint=self.endpoint,
            config=self._config)

    @property
    def endpoint(self):
        """Get endpoint"""
        return utils.normalize_endpoint(
            self._config.base_url,
            self.ENDPOINT.format(project_id=self._config.project_id))

    @property
    def payload_type(self):
        """Get payload type"""
        return self.REQUEST_PAYLOAD_TYPE

    def _create_payload(self, data: Any) -> RequestPayload:
        """Create payload based on payload type"""
        return self.payload_type(data)
