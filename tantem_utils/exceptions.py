"""Exceptions"""


class TantemUtilsError(Exception):
    """TantemUtilsError"""


class TestcaseUpdaterError(TantemUtilsError):
    """TestcaseUpdaterError"""
    __test__ = False


class ParserError(TestcaseUpdaterError):
    """ParserError"""


class ReaderError(TestcaseUpdaterError):
    """ReaderError"""


class ProducerError(TestcaseUpdaterError):
    """ProducerError"""


class InvalidPathError(TestcaseUpdaterError):
    """InvalidPathError"""


class SenderError(TantemUtilsError):
    """SenderError"""


class FeatureFileNotFoundError(TantemUtilsError):
    """FeatureFileNotFoundError"""


class AuthError(TantemUtilsError):
    """AuthError"""


class RequestError(TantemUtilsError):
    """Request exception"""


class NotifierError(TantemUtilsError):
    """NotifierError"""


class DataError(TantemUtilsError):
    """DataError"""


class ConfigurationError(TantemUtilsError):
    """ConfigurationError"""


class ReportUploaderError(TantemUtilsError):
    """ReportUploaderError"""


class ExtractorError(TantemUtilsError):
    """ExtractorError"""
