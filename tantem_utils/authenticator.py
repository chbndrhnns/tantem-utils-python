"""Authenticator class handles the authentication with Tantem"""
import logging

from tantem_utils import exceptions as err
from tantem_utils.http import HttpRequestAdapter
from tantem_utils.dtos import ResponsePayload
from tantem_utils.config import Configuration

logger = logging.getLogger(__name__)  # pylint: disable=invalid-name


class Authenticator:  # pylint: disable=too-few-public-methods
    """Authenticator class"""

    AUTH_ENDPOINT = '/tim/auth/tokens'

    def __init__(
            self,
            config: Configuration) -> None:
        self._token = None
        self._config: Configuration = config
        self._http = HttpRequestAdapter(
            endpoint=self.endpoint,
            auth=self._config.auth_data,
            verify_ssl=config.verify_ssl
        )

    @property
    def endpoint(self) -> str:
        """Get endpoint"""
        return f'{self._config.base_url}{self.AUTH_ENDPOINT}'

    @property
    def token(self) -> str:
        """Get token"""
        return self._token or self._retrieve_token()

    @token.setter
    def token(self, val: str) -> None:
        """Set token"""
        self._token = val

    def _retrieve_token(self) -> str:
        """Retrieve a token from the authentication layer"""
        logger.info('Retrieving token...')
        try:
            res: ResponsePayload = self._http.post(
                headers={
                    'accept': 'application/json'
                }
            )
            data = res.json
            if res.status_code >= 401:
                raise err.RequestError('Incorrect credentials.')
            self.token = data.get('token')
            logger.debug('Received token: %s', self.token)
            return self.token
        except err.RequestError as exc:
            raise err.AuthError(f'Could not authenticate: {exc}')
