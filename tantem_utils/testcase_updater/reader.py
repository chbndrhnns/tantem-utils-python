"""reader module"""
import glob
import os
import logging

from tantem_utils import exceptions as err

logger = logging.getLogger(__name__)  # pylint: disable=invalid-name


class Reader:
    """Reader class"""

    def __init__(self, path: str) -> None:
        """Reader constructor

        Attributes:
          path (str): Path where to look for feature files
        """
        self._path: str = path
        self.files: dict = {}

    @property
    def path(self) -> str:
        """Getter for path"""
        return self._path

    @path.setter
    def path(self, val: str) -> None:
        """Setter for path"""
        if not self._path_exists(val):
            raise err.InvalidPathError(f'Path does not exist: {val}')
        self._path = val

    @classmethod
    def _path_exists(cls, path: str) -> bool:
        """Return True if the path exists, otherwise False"""

        return os.path.exists(path)

    @classmethod
    def file_exists(cls, file_name: str) -> bool:
        """Returns True if a file exists, otherwise returns False"""

        try:
            return os.path.isfile(file_name)
        except TypeError:
            return False

    def get_files(self) -> list:
        """Get list of feature files in the directory"""

        logger.info('Retrieving list of files from %s...', self.path)
        res = glob.glob(f'{self.path}/**/*.feature', recursive=True)
        logger.debug(res)
        return res

    def read_files(self, files: list = None) -> dict:
        """Reads a list of files in to a dictionary"""

        result = {}

        if not files:
            files = self.get_files()

        if not files:
            raise err.ReaderError(f'No feature files found at {self.path}')

        logger.info('Reading files...')
        for file_name in files:
            result[file_name] = self._read_file(file_name)

        self.files = result
        return result

    def _read_file(self, file_name: str) -> str:
        """Reads a file and returns it as string"""

        if not self.file_exists(file_name):
            raise err.FeatureFileNotFoundError(
                f'Could not find file "{file_name}"')

        with open(file_name, 'r') as obj:
            content = obj.read()

        return content
