"""JSON producer"""
import logging
logger = logging.getLogger(__name__)  # pylint: disable=invalid-name


class JsonProducer():
    """JsonProducer class

    Takes the output of the parser and transforms it into JSON data for Tantem"""

    def __init__(self, data: dict, file_name: str) -> None:
        self.data = data or {}
        self.file_name = file_name

        self.feature = self._get_feature()
        self.scenarios = self._get_scenarios()

    def __str__(self):
        """__str__"""

    def transform(self) -> list:
        """Transforms the parsed features and scenarios into JSON according to the contract"""

        transformed = dict(
            **self._transform_feature(),
            testCases=[self._transform_scenario(s) for s in self.scenarios],
            suiteLocation=self._get_suite_location()
        )
        logger.debug(transformed)
        return transformed

    def _get_suite_location(self) -> str:
        """Get the relative location of the feature file on the file system"""
        return self.file_name

    def _transform_feature(self) -> dict:
        """Transforms a feature"""

        return {
            'tags': self._get_tags(self.feature),
            'suiteId': self.feature.get('name'),
            'suiteName': self.feature.get('name'),
        }

    def _transform_scenario(self, scenario: dict) -> dict:
        """Transforms a scenario"""

        logger.debug('Scenario before transformation: %s', scenario)

        transformed = {
            "testCaseName": scenario.get('name'),
            "testCaseId": scenario.get('name'),
            "tags": self._get_tags(scenario)}

        logger.debug('Scenario after transformation: %s', transformed)

        return transformed

    def _get_tags(self, data: dict) -> list:
        """Return list of tags from a data structure"""

        tags = set()

        # add tags from feature
        tags.update([tag.get('name')
                     for tag in self._get_feature().get('tags')])

        # add tags from scenario
        tags.update([tag.get('name') for tag in data.get('tags', [])])

        return sorted(list(tags))

    def _get_feature(self) -> dict:
        """Returns the feature"""

        return self.data.get('feature', {})

    def _get_scenarios(self, feature: dict = None) -> list:
        """Returns the name of the scenarios"""

        if not feature:
            feature = self._get_feature()

        return [s for s in feature.get('children')]
