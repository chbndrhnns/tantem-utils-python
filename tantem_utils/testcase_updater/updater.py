"""Module that creates a list of test cases for Tantem"""

import logging
from typing import List, Dict, Type

from tantem_utils.config import Configuration
from tantem_utils.dtos import DictRequestPayload
from tantem_utils.testcase_updater.reader import Reader
from tantem_utils.testcase_updater.parser import GherkinParser
from tantem_utils.testcase_updater.producer import JsonProducer
from tantem_utils import exceptions as err
from tantem_utils.utility import Utility

logger = logging.getLogger(__name__)  # pylint: disable=invalid-name


class TestcaseUpdater(Utility):  # pylint: disable=too-few-public-methods
    """TestcaseUpdater class"""

    ENDPOINT: str = '/tim/testautomationmicrot/api/project/{project_id}/test/case'
    REQUEST_PAYLOAD_TYPE: Type[DictRequestPayload] = DictRequestPayload

    def __init__(self, config: Configuration):
        super().__init__(config)

        self._files: Dict = {}
        self._data: List = []
        self._enriched_payload: Dict = {}

    def enrich_payload(self, data: list):
        """Adds Tantem-specific data to the payload so that it matches the contract"""
        return dict(projectId=self._config.project_id, data=data)

    def run(self, dry_run: bool = False) -> True:
        """Running the updater consists of several steps:

        1. Reading the files
        2. Parsing the files
        3. Transforming the relevant data according to the contract
        4. Sending the transformed data"""

        self._files: Dict = self._get_files()

        for name, content in self._files.items():
            logger.info('Processing %s', name)
            parsed_content = GherkinParser().parse(content)
            self._data.append(JsonProducer(parsed_content, name).transform())

        self._enriched_payload: Dict = self._process_data(self._data)
        if not dry_run:
            self._send_data(self._enriched_payload)

        return True

    def _send_data(self, data: Dict) -> None:
        """Send data to server"""
        logger.info('Sending data...')

        try:
            payload = DictRequestPayload(data=data)
            self._sender.send(payload)
        except err.SenderError as exc:
            logger.error(exc)
            raise err.TestcaseUpdaterError(f'Sending data failed: {exc}')

    def _process_data(self, data: List) -> Dict:
        """Add Tantem-specific data to the dictionary"""
        logger.info('Processing data...')

        enriched_payload = self.enrich_payload(data)
        return enriched_payload

    def _get_files(self) -> Dict:
        """Read files from filesystem"""
        logger.info('Reading files...')

        files = Reader(path=self._config.tests_location).read_files()
        return files
