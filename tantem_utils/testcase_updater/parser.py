"""Gherkin parser"""
from gherkin import parser, token_scanner, errors

from tantem_utils import exceptions as exc


class GherkinParser:
    """Gherkin parser class"""

    def __init__(self):
        """Constructor"""
        self._parser = parser.Parser()

    def __str__(self):
        """__str__"""

    def parse(self, inp: str = None) -> dict:
        """Parse a string"""

        scanner = token_scanner.TokenScanner(inp)
        try:
            return self._parser.parse(scanner)
        except errors.CompositeParserException as err:
            raise exc.ParserError(err)
