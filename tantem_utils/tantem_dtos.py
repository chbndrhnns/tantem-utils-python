"""Dataclasses for data that needs to be sent to Tantem"""
# pylint: disable=too-few-public-methods
from dataclasses import dataclass


class TantemData:
    """
    Base class for data that is sent to Tantem

    Its main characteristic is that we need to convert Python snake_case to Java camelCase
    """


@dataclass
class EventData(TantemData):
    """Event dataclass"""
    pipeline_id: int
    test_case_id: str
    status: str = 'stopped'
    detailed_report_url: str = ''
    is_stopped: bool = False
    log: str = ''


@dataclass
class ReportData(TantemData):
    """Report dataclass"""
    pipeline_id: int
    content: str = ''
