"""Configuration"""
import os
import logging

from tantem_utils import exceptions as err
from tantem_utils.utils import str2bool

logger = logging.getLogger(__name__)  # pylint: disable=invalid-name


class Configuration:
    """Configuration class"""

    __slots__ = ['_config']
    env_vars = {
        'CI_PROJECT_ID': None,
        'CI_PIPELINE_ID': None,
        'REPORT_LOCATION': 'artifacts/service-tests-json/results.json',
        'SSL_VERIFY': 'True',
        'TANTEM_AUTH_USER': None,
        'TANTEM_AUTH_PASSWORD': None,
        'TANTEM_BASE_URL': None,
        'TESTS_LOCATION': 'features/'
    }

    def __init__(self, from_dict: dict = None):
        self._config: dict = from_dict or {}

        if not from_dict:
            self._discover_env_vars()
        self._log_config()

    @property
    def pipeline_id(self):
        """Get pipeline id"""
        return self._config.get('CI_PIPELINE_ID')

    @property
    def project_id(self):
        """Get project id"""
        return self._config.get('CI_PROJECT_ID')

    @property
    def report_location(self):
        """Get report location"""
        return self._config.get('REPORT_LOCATION')

    @property
    def auth_user(self):
        """Get username for authentication"""
        return self._config.get('TANTEM_AUTH_USER')

    @property
    def auth_password(self):
        """Get password for authentication"""
        return self._config.get('TANTEM_AUTH_PASSWORD')

    @property
    def base_url(self):
        """Get base url

        This is equal to the environment we are running in"""
        return self._config.get('TANTEM_BASE_URL')

    @property
    def tests_location(self):
        """Get path where the tests are located"""
        return self._config.get('TESTS_LOCATION')

    @property
    def verify_ssl(self):
        """Get SSL_VERIFY flag"""
        return str2bool(self._config.get('SSL_VERIFY'))

    def _discover_env_vars(self):
        """Retrieves the values of environment variables"""
        missing: list = []
        for var, default in self.env_vars.items():
            val = os.getenv(var, default) or self._config.get(var)
            if val is None:
                missing.append(var)
                logger.error('Variable %s not set.', var)

            self._config[var] = val.strip() if isinstance(val, str) else val

        if missing:
            raise err.ConfigurationError(
                f'Environment variables missing: {missing}. Exiting...')

    def _log_config(self):
        """Log the current configuration"""

        logger.info('Running with configuration:')
        for var, val in self._config.items():
            if 'PASSWORD' in var or 'TOKEN' in var:
                logger.info('%s: %s', var, '#MASKED#')
            else:
                logger.info('%s: %s', var, val or '<not set>')

    @property
    def auth_data(self) -> dict:
        """Generates a dictionary that contains the authentication details"""
        return {
            'username': self.auth_user,
            'password': self.auth_password
        }
