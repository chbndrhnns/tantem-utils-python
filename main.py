"""main.py"""

import sys

from tantem_utils.config import Configuration
from tantem_utils import TestcaseUpdater
from tantem_utils.exceptions import TestcaseUpdaterError


def exit(code, message):
    print(f'Exiting: {message}')
    sys.exit(code)


def main():
    """Main function to run the update process"""
    try:
        config: Configuration = Configuration()
        TestcaseUpdater(config=config).run()
    except TestcaseUpdaterError:
        exit(1, f'Error')

    exit(0, 'Update successful')


if __name__ == '__main__':
    main()
