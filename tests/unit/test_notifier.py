"""Tests for the Notifier class"""
import json
from urllib import parse

import pytest
from pytest_mock import MockFixture

from tantem_utils import exceptions as err, utils
from tantem_utils.authenticator import Authenticator
from tantem_utils.config import Configuration
from tantem_utils.dtos import EventDataRequestPayload
from tantem_utils.exceptions import SenderError
from tantem_utils.notifier.notifier import Notifier, logger
from tantem_utils.notifier.dtos import TestCaseStartedEvent, TestCaseStoppedEvent
from tantem_utils.tantem_dtos import EventData
from tantem_utils.sender import Sender
from tests.unit.conftest import mock_response


@pytest.fixture
def notifier(
        config: Configuration,
        stub_auth: Authenticator,
        stub_sender: Sender) -> Notifier:
    notifier: Notifier = Notifier(config=config)
    notifier._sender = stub_sender
    notifier._sender._authenticator = stub_auth

    return notifier


def test__emit_invalid_type(notifier):
    with pytest.raises(err.DataError):
        notifier.emit('str')


def test__notifier_has_base_config(notifier: Notifier, auth_data: dict):
    # assert
    assert 'report/job' in notifier.endpoint
    assert isinstance(notifier._config, Configuration)
    assert isinstance(notifier._sender, Sender)


def test__logger_exists(caplog, notifier: Notifier):
    # arrange
    expected = 'test log'

    # act
    logger.error(expected)

    # assert
    assert expected in caplog.messages


def test__endpoint_contains_url(notifier: Notifier):
    # act
    endpoint: str = notifier.endpoint

    # assert
    parsed_endpoint = parse.urlparse(endpoint)
    assert parsed_endpoint.scheme
    assert parsed_endpoint.netloc
    assert parsed_endpoint.path


def test__set_event_details__report_url(event_started: TestCaseStartedEvent):
    # act
    url: str = f'https://localhost/projects/1234'
    details = {
        'detailed_report_url': url
    }
    event_started.set_event_details(**details)

    # assert
    e: EventData = event_started.data
    assert url in e.detailed_report_url


def test__TestCaseStartedEvent(event_started_data):
    # arrange
    event = TestCaseStartedEvent(data=event_started_data)

    # assert
    assert isinstance(event, TestCaseStartedEvent)
    assert isinstance(event._data, EventData)

    assert event.data.status, 'started'


def test__TestCaseStoppedEvent(event_stopped_data):
    # arrange
    event = TestCaseStoppedEvent(data=event_stopped_data)

    # assert
    assert isinstance(event, TestCaseStoppedEvent)
    assert isinstance(event._data, EventData)

    assert event.data.status, 'stopped'


def test__EventData():
    # arrange
    assert EventData(pipeline_id=123, test_case_id='123', status='running')


def test__send_notification(
        mocker: MockFixture,
        event_started: TestCaseStartedEvent,
        notifier: Notifier):
    # arrange
    mocker.patch.object(
        notifier._sender._http, '_send',
        side_effect=mock_response(content='{"msg": "ok"}'))
    mocker.spy(notifier._sender, 'send')

    # act
    notifier.emit(event_started)

    # assert
    sent = notifier._create_payload(event_started.data)
    notifier._sender.send.assert_called_once_with(sent)


def test__convert_event_payload_to_camelCase(
        event_started: TestCaseStartedEvent,
        event_data_request_payload: EventDataRequestPayload):
    # arrange
    expected_keys = {'isStopped', 'detailedReportUrl'}
    payload = EventDataRequestPayload(data=event_started.data)

    # act
    actual = payload.to_json()

    # assert
    as_json: dict = json.loads(actual)
    actual_keys = set(as_json.keys())
    assert expected_keys.issubset(actual_keys)


def test__testcase_started_shortcut(mocker: MockFixture, notifier: Notifier):
    # arrange
    mocker.spy(notifier, 'emit')

    # act
    notifier.test_case_started(test_case_id='abc 1')

    # assert
    notifier.emit.assert_called_once()


def test__testcase_stopped_shortcut(mocker: MockFixture, notifier: Notifier):
    # arrange
    mocker.spy(notifier, 'emit')

    # act
    notifier.test_case_stopped(test_case_id='abc 1')

    # assert
    notifier.emit.assert_called_once()


def test_test_case_stopped(mocker: MockFixture, notifier: Notifier):
    # arrange
    mocker.spy(notifier, 'emit')
    mocker.spy(utils, 'handle_silent_error')
    mocker.patch.object(notifier._sender, 'send', side_effect=SenderError())

    # act
    notifier.test_case_stopped('id')

    #
    notifier.emit.assert_called_once()
    utils.handle_silent_error.assert_called_once()


def test_test_case_started(mocker: MockFixture, notifier: Notifier):
    # arrange
    mocker.spy(notifier, 'emit')
    mocker.spy(utils, 'handle_silent_error')
    mocker.patch.object(notifier._sender, 'send', side_effect=SenderError())

    # act
    notifier.test_case_started('id')

    #
    notifier.emit.assert_called_once()
    utils.handle_silent_error.assert_called_once()
