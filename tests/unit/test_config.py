import logging
import os

import pytest

from tantem_utils.config import Configuration
from tantem_utils import exceptions as err

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

logger = logging.getLogger(__name__)  # pylint: disable=invalid-name


@pytest.fixture
def config_dict() -> dict:
    return {
        'CI_PROJECT_ID': 3416,
        'CI_PIPELINE_ID': 54321,
        'REPORT_LOCATION': 'artifacts/service-tests-json/results.json',
        'SSL_VERIFY': 'True',
        'TANTEM_AUTH_USER': 'demo-user',
        'TANTEM_AUTH_PASSWORD': 'demo',
        'TANTEM_BASE_URL': 'https://tantem-dev.ctf.in.pan-net.eu',
        'TESTS_LOCATION': os.path.realpath(
            os.path.join(
                ROOT_DIR,
                '..',
                'features'))}


def test__mask_passwords_in_output(caplog, set_env_vars):
    # act
    Configuration()

    # assert
    lines = caplog.messages
    password_line = [msg for msg in lines if 'TANTEM_AUTH_PASSWORD' in msg][0]
    assert 'PASSWORD: #MASKED#' in password_line


def test__missing_variable(set_env_vars, monkeypatch):
    # arrange
    monkeypatch.delenv('CI_PROJECT_ID')

    # act, assert
    with pytest.raises(err.ConfigurationError):
        Configuration()


def test___discover_env_vars(set_env_vars):
    # arrange, act
    c: Configuration = Configuration()

    # assert
    assert c.base_url
    assert c.auth_password
    assert c.auth_user
    assert c.tests_location
    assert c.project_id
    assert c.verify_ssl
    assert c.pipeline_id


def test__ssl_verification__on_by_default(set_env_vars):
    assert Configuration().verify_ssl


def test__ssl_verification__switched_off(set_env_vars, monkeypatch):
    monkeypatch.setenv('SSL_VERIFY', 'False')
    c = Configuration()
    assert not c.verify_ssl


def test__config_from_dict(config_dict: dict):
    # arrange
    config_dict.update({'SSL_VERIFY': 'False'})

    # act
    c = Configuration(from_dict=config_dict)

    # assert
    assert not c.verify_ssl


def test_strip_spaces_from_env_vars(monkeypatch, set_env_vars):
    # arrange
    expected = '12345'
    monkeypatch.setenv('CI_PROJECT_ID', f' {expected} ')

    # act
    c = Configuration()

    # assert
    assert c.project_id == expected
