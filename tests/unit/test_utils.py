"""Tests for utils"""
import json
import logging

from tantem_utils import utils
from tantem_utils.exceptions import TantemUtilsError

from tantem_utils.tantem_dtos import EventData
from tantem_utils.utils import SnakeToCamelCaseJSONEncoder, str2bool


def test__encode_event_with_camelCase_keys():
    # arrange
    inp = EventData(pipeline_id=123, test_case_id='abc testcase')

    # act
    raw_json = json.dumps(inp, cls=SnakeToCamelCaseJSONEncoder)

    # assert
    assert isinstance(raw_json, str)
    parsed: dict = json.loads(raw_json)
    assert 'pipelineId' in parsed.keys()
    assert 'isStopped' in parsed.keys()


def test__encode_default():
    assert json.dumps({'a': 'b'}, cls=SnakeToCamelCaseJSONEncoder)


def test_str2bool():
    assert str2bool('False') == False
    assert str2bool('True')


def test_handle_silent_error(caplog):
    # arrange
    expected = 'Bla try again'
    logger = logging.getLogger(__name__)
    exc = TantemUtilsError(expected)

    # act
    utils.handle_silent_error(logger, exc)

    # assert
    assert expected in caplog.text
    for r in caplog.records:
        assert r.levelno == logging.WARNING
