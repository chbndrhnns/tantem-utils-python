"""Tests for Authenticator class"""
import json
import os
import uuid

import pytest
from pytest_mock import MockFixture

from tantem_utils.config import Configuration
from tantem_utils.dtos import ResponsePayload
from tantem_utils.sender import err
from tantem_utils.authenticator import Authenticator


@pytest.fixture
def authenticator(config) -> Authenticator:
    user = os.getenv('TANTEM_AUTH_USER')
    password = os.getenv('TANTEM_AUTH_PASSWORD')
    a = Authenticator(config=config)
    return a


@pytest.fixture
def dummy_token() -> str:
    return "dummy-token"


def token_response(token: str = None) -> ResponsePayload:
    content = {
        'token': token or str(uuid.uuid4())
    }
    yield ResponsePayload(content=json.dumps(content))


def invalid_token_response() -> ResponsePayload:
    yield ResponsePayload(status_code=401)


def test__get_token__auth_error(
        mocker: MockFixture,
        authenticator: Authenticator):
    # arrange
    mocker.patch.object(
        authenticator._http,
        'post',
        side_effect=invalid_token_response())

    # act, assert
    with pytest.raises(err.AuthError, match='credentials'):
        authenticator._retrieve_token()


def test__get_token__pass(
        mocker: MockFixture,
        authenticator: Authenticator,
        dummy_token: str):
    # arrange
    mocker.patch.object(
        authenticator._http,
        'post',
        side_effect=token_response(dummy_token))

    # act
    assert authenticator.token == dummy_token


def test__ssl_verify_false(set_env_vars, monkeypatch):
    # arrange
    monkeypatch.setenv('SSL_VERIFY', 'False')
    config: Configuration = Configuration()

    # act
    a: Authenticator = Authenticator(config=config)

    # assert
    assert not a._http.ssl_verify


def test__store_token_for_reuse(
        mocker: MockFixture,
        authenticator: Authenticator,
        dummy_token):
    # arrange
    mocker.patch.object(
        authenticator._http,
        'post',
        side_effect=token_response())

    # act
    first_token = authenticator.token

    # we need to reset the side effect
    mocker.patch.object(
        authenticator._http,
        'post',
        side_effect=token_response())
    second_token = authenticator.token

    # assert
    assert first_token == second_token
