"""Utils for tests"""
import os
import uuid

import pytest

from tantem_utils.dtos import ResponsePayload
from tantem_utils.testcase_updater.updater import TestcaseUpdater


def create_feature_file(path, content=''):
    file_name = f'dummy-{uuid.uuid4().hex}.feature'
    feature_file = os.path.join(path, file_name)
    with open(feature_file, 'a') as f:
        f.write(content)
    return file_name


def valid_response() -> ResponsePayload:
    yield ResponsePayload(
        content="",
        status_code=200,
        headers={
            'content-type': 'application/json'
        }
    )


def valid_response_empty_payload() -> ResponsePayload:
    yield ResponsePayload(
        content=None,
        status_code=200
    )


def invalid_response() -> ResponsePayload:
    r = ResponsePayload(status_code=401, content="")
    yield r


@pytest.fixture
def unset_env_vars(monkeypatch):
    for var in TestcaseUpdater.config:
        monkeypatch.delenv(var, raising=False)
