"""Tests for Utility class"""

import pytest

from tantem_utils.config import Configuration
from tantem_utils.dtos import RequestPayload
from tantem_utils.utility import Utility


@pytest.fixture
def utility(config) -> Utility:
    return Utility(config=config)


def test__has_payload_type(utility: Utility):
    assert utility.payload_type


def test__create_payload_according_to_type(utility):
    # arrange
    data = {'a': 'b'}

    # act
    res = utility._create_payload(data)

    # assert
    assert isinstance(res, RequestPayload)
    assert res.to_json() == '{"a": "b"}'


def test__configuration_is_created_if_not_injected(set_env_vars):
    # act
    utility: Utility = Utility()

    # assert
    assert isinstance(utility._config, Configuration)
