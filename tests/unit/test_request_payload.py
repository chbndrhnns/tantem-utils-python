"""Tests for the RequestPayload object"""
import json

import pytest

from tantem_utils import exceptions as err
from tantem_utils.dtos import RequestPayload, DictRequestPayload, EventDataRequestPayload, EmptyRequestPayload


@pytest.fixture
def dict_payload() -> RequestPayload:
    return DictRequestPayload(data={'a': 'b'})


def test_create_EventDataRequestPayload_with_invalid_data():
    with pytest.raises(err.DataError):
        EventDataRequestPayload(data='str')


def test_create_DictRequestPayload_with_invalid_data():
    with pytest.raises(err.DataError):
        DictRequestPayload(data='str')


def test_convert_dict_to_json(dict_payload: DictRequestPayload):
    # act
    actual = dict_payload.to_json()

    # assert
    assert isinstance(actual, str)
    assert json.loads(actual)


def test_convert_event_data_to_json(
        event_data_request_payload: EventDataRequestPayload):
    # act
    actual = event_data_request_payload.to_json()

    # assert
    d = json.loads(actual)
    assert d.get('isStopped') == False


def test_empty_data_returns_None_when_converted_to_json():
    # arrange
    empty: RequestPayload = EmptyRequestPayload()

    # act
    actual = empty.to_json()

    # assert
    assert actual is None
