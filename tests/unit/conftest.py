"""conftest.py for unit tests"""
from typing import Any

import pytest

from tantem_utils.dtos import ResponsePayload


@pytest.fixture
def feature():
    return ('Demo 1', """
    @feature_tag
    Feature: Demo 1

      Scenario: Case 1

      @ping
      Scenario: Case 2
  """)


@pytest.fixture
def transformed_scenarios():
    return [
        {
            "testCaseName": "Case 1",
            "testCaseId": "Case 1",
            "tags": [
                "@feature_tag"
            ]
        },
        {
            "testCaseName": "Case 2",
            "testCaseId": "Case 2",
            "tags": [
                "@feature_tag",
                "@ping"
            ]
        }
    ]


@pytest.fixture
def auth_data() -> dict:
    return {
        'user': 'dummy-user',
        'password': 'noRealPassword'
    }


def mock_response(
        content: Any = None,
        status_code: int = 200,
        response_headers: dict = None):
    """Response from the HttpRequestAdapter"""
    r = ResponsePayload(status_code=status_code,
                        content=content)
    r.headers = response_headers or {'content-type': 'application/json'}
    yield r
