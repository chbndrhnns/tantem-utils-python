"""Tests for the requests adapter"""
import json as json_lib
from typing import Any

import pytest
import requests
from requests.auth import HTTPBasicAuth
from pytest_mock import MockFixture

from tantem_utils import exceptions as err
from tantem_utils.http import HttpRequestAdapter
from tantem_utils.dtos import ResponsePayload, RequestPayload, DictRequestPayload
from tests.unit.conftest import mock_response


def response(
        content: Any = None,
        status_code: int = 200,
        response_headers: dict = None):
    """Response from the requests library"""
    r: requests.Response = requests.Response()
    r.status_code = status_code
    r.headers = response_headers
    yield r


def request_exception_with_response_body():
    res = requests.Response()
    res._content = b'{"timestamp":"2019-09-13T09:43:29.938+0000","status":400,"error":"Bad Request","message":"JSON parse error"}'
    yield requests.HTTPError(
        response=res
    )


@pytest.fixture
def host() -> str:
    return 'http://doesdefintetetetetelynotexist.local'


@pytest.fixture
def adapter(host):
    return HttpRequestAdapter(host)


@pytest.fixture
def basic_auth_data():
    return {'username': 'testuser', 'password': 'testpassword'}


@pytest.fixture
def request_payload() -> RequestPayload:
    return DictRequestPayload(data={'a': 'b'})


def test__send_get_request__returns_Response(
        mocker: MockFixture,
        adapter: HttpRequestAdapter):
    # act
    mocker.patch('tantem_utils.http.requests.get', side_effect=response())
    res = adapter.get()

    # assert
    assert isinstance(res, ResponsePayload)


def test__send_get_request__ok(
        mocker: MockFixture,
        adapter: HttpRequestAdapter):
    # arrange
    expected = 'dummy'
    mocker.patch.object(adapter, 'get', side_effect=mock_response(expected))

    # act
    res: ResponsePayload = adapter.get()

    # assert
    assert res.content == expected
    assert res.content_type == 'application/json'


def test__send_get_request__json(
        mocker: MockFixture,
        adapter: HttpRequestAdapter):
    # arrange
    expected = '{"msg": "ok"}'
    mocker.patch.object(
        adapter,
        'get',
        side_effect=mock_response(
            content=expected))

    # act
    res: ResponsePayload = adapter.get()

    # assert
    assert res.json == json_lib.loads(expected)


def test__send_get_request__200(
        mocker: MockFixture,
        adapter: HttpRequestAdapter):
    # arrange
    expected = 200
    mocker.patch.object(
        adapter, 'get', side_effect=mock_response(
            status_code=expected))

    # act
    res: ResponsePayload = adapter.get()

    # assert
    assert res.status_code == expected


def test__send_get_request__raise_for_status(
        mocker: MockFixture,
        adapter: HttpRequestAdapter):
    # arrange
    mocker.patch.object(
        adapter,
        'get',
        side_effect=mock_response(
            status_code=400))

    # act
    with pytest.raises(err.RequestError):
        res: ResponsePayload = adapter.get()
        res.raise_for_status()


def test__send_put_request(mocker: MockFixture, adapter: HttpRequestAdapter):
    # arrange
    mocker.patch('tantem_utils.http.requests.put', side_effect=response())

    # act
    res: ResponsePayload = adapter.put()

    # assert
    assert res.status_code == 200


def test__send_post_request__200(
        mocker: MockFixture,
        adapter: HttpRequestAdapter):
    # arrange
    expected = 200
    mocker.patch.object(
        adapter,
        'post',
        side_effect=mock_response(
            status_code=expected))

    # act
    res: ResponsePayload = adapter.post()

    # assert
    assert res.status_code == expected


def test__send_get_request_to_endpoint(
        mocker: MockFixture,
        adapter: HttpRequestAdapter,
        host: str):
    # arrange
    mocker.patch.object(adapter, 'get', side_effect=mock_response())

    # act
    res: ResponsePayload = adapter.get()

    # assert
    assert res.status_code == 200
    assert host in adapter.endpoint


def test__send_invalid_verb(adapter: HttpRequestAdapter):
    # act
    with pytest.raises(err.RequestError, match='verb'):
        adapter._send('bla')


def test__send_without_endpoint_fails(adapter: HttpRequestAdapter):
    adapter.endpoint = None
    # act
    with pytest.raises(err.RequestError):
        adapter.get()


def test__send_get_request_with_headers(
        mocker: MockFixture,
        adapter: HttpRequestAdapter):
    # arrange
    mocker.patch('tantem_utils.http.requests.get', side_effect=response())
    mocker.spy(adapter, '_send')
    headers = {
        'X-Token': '123'
    }

    # act
    adapter.get(headers=headers)

    # assert
    assert adapter._send.call_count == 1
    adapter._send.assert_called_with(auth=None, headers=headers, verb='get')


def test__send_post_request_with_json_body(
        mocker: MockFixture,
        request_payload: RequestPayload,
        adapter: HttpRequestAdapter):
    # arrange
    mocker.patch('tantem_utils.http.requests.post', side_effect=response())
    mocker.spy(adapter, '_send')

    # act
    adapter.post(payload=request_payload)

    # assert
    assert adapter._send.call_count == 1
    adapter._send.assert_called_with(
        payload=request_payload, auth=None, verb='post', headers=None)


def test__basic_auth(adapter: HttpRequestAdapter, basic_auth_data: dict):
    # act
    adapter.auth = basic_auth_data

    # assert
    assert isinstance(adapter._auth, HTTPBasicAuth)
    assert adapter._auth.username == basic_auth_data.get('username')
    assert adapter._auth.password == basic_auth_data.get('password')


def test__basic_auth_in_constructor(
        adapter: HttpRequestAdapter,
        basic_auth_data: dict):
    # arrange, act
    adapter = HttpRequestAdapter(
        endpoint='localhost',
        auth=basic_auth_data
    )

    # assert
    assert isinstance(adapter._auth, HTTPBasicAuth)


def test__response_has_content_type(
        adapter: HttpRequestAdapter,
        mocker: MockFixture):
    # arrange
    expected_headers = {
        'content-type': 'application/json'
    }
    mocker.patch(
        'tantem_utils.http.requests.get',
        side_effect=response(
            response_headers=expected_headers))

    # act
    res: ResponsePayload = adapter.get()

    # assert
    assert isinstance(res.headers, dict)
    assert res.headers.get(
        'content-type') == expected_headers.get('content-type')


def test__request_exception_get_response_message(
        adapter: HttpRequestAdapter, mocker: MockFixture):
    # arrange
    mocker.patch('tantem_utils.http.requests.get',
                 side_effect=request_exception_with_response_body())

    # act
    with pytest.raises(err.RequestError, match='timestamp'):
        adapter.get()


def test__validate_payload_format__pass(
        adapter: HttpRequestAdapter):
    # act, assert
    assert adapter._is_valid_payload(data={})


def test__validate_payload_format__fail(
        adapter: HttpRequestAdapter,
):
    # act, assert
    assert not adapter._is_valid_payload(data='string')


def test__ResponsePayload_decode_invalid_json_fails():
    # arrange
    r: ResponsePayload = ResponsePayload()
    r._content = 'stringy'

    # act, assert
    with pytest.raises(err.DataError):
        r.json


def test__fail_without_url():
    # act
    with pytest.raises(err.RequestError, match='endpoint'):
        HttpRequestAdapter(endpoint='')


def test_ssl_verify_setting(adapter: HttpRequestAdapter):
    assert adapter.ssl_verify


def test_set_ssl_verify_setting():
    # arrange
    adapter: HttpRequestAdapter = HttpRequestAdapter(
        endpoint='localhost', verify_ssl=False)

    # assert
    assert not adapter.ssl_verify
