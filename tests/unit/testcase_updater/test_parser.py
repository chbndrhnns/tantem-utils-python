# Tests for the parser class
import pytest
from collections import abc

from tantem_utils.testcase_updater.parser import GherkinParser
from tantem_utils.exceptions import ParserError


@pytest.fixture
def invalid_feature():
    return (ParserError, """
    Featured: Demo 1

Bla
  """)


def test__parse_gherkin(feature):
    parser = GherkinParser()

    result = parser.parse(feature[1])
    assert isinstance(result, abc.Mapping)

    assert result['feature']['name'] == feature[0]


def test__parse_gherkin__invalid__exception(invalid_feature):
    parser = GherkinParser()

    with pytest.raises(invalid_feature[0]):
        parser.parse(invalid_feature[1])
