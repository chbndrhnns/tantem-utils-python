"""Tests for JsonProducer"""
import pytest
import json

from tantem_utils.testcase_updater.producer import JsonProducer


@pytest.fixture
def parsed_feature():
    return {'type': 'GherkinDocument',
            'feature': {'type': 'Feature',
                        'tags': [{"type": "Tag",
                                  "location": {"line": 1,
                                               "column": 1},
                                  "name": "@feature_tag"}],
                        'location': {'line': 2,
                                     'column': 5},
                        'language': 'en',
                        'keyword': 'Feature',
                        'name': 'Demo 1',
                        'children': [{'type': 'Scenario',
                                      'tags': [],
                                      'location': {'line': 4,
                                                   'column': 7},
                                      'keyword': 'Scenario',
                                      'name': 'Case 1',
                                      'steps': []},
                                     {'type': 'Scenario',
                                      'tags': [{"type": "Tag",
                                                "location": {"line": 5,
                                                             "column": 7},
                                                "name": "@ping"}],
                                      'location': {'line': 6,
                                                   'column': 7},
                                      'keyword': 'Scenario',
                                      'name': 'Case 2',
                                      'steps': []}]},
            'comments': []}


@pytest.fixture
def transformed_feature():
    return {
        "suiteName": "Demo 1",
        "suiteId": "Demo 1",
        "tags": [
            "@feature_tag"
        ]
    }


@pytest.fixture
def scenario():
    return {
        'type': 'Scenario',
        'tags': [{"type": "Tag",
                  "location": {"line": 5,
                               "column": 7},
                  "name": "@ping"}],
        'location': {
            'line': 4,
            'column': 7},
        'keyword': 'Scenario',
        'name': 'Case 2',
        'steps': []}


@pytest.fixture
def transformed_scenario():
    return {
        "testCaseName": "Case 2",
        "testCaseId": "Case 2",
        "tags": [
            "@feature_tag",
            "@ping"
        ]
    }


@pytest.fixture
def scenario_tags():
    return ["@ping"]


@pytest.fixture
def feature_file_name():
    return 'features/dummy.feature'


@pytest.fixture
def producer(parsed_feature, feature_file_name):
    return JsonProducer(parsed_feature, feature_file_name)


@pytest.fixture
def transformed_output(
        transformed_feature,
        transformed_scenarios,
        feature_file_name):
    output = {}
    output.update(transformed_feature)
    output['testCases'] = transformed_scenarios
    output['suiteLocation'] = feature_file_name
    return output


def test___get_scenarios(producer):
    expected = ['Case 1', 'Case 2']
    actual = [s.get('name') for s in producer._get_scenarios()]

    assert set(expected) == set(actual)


def test___get_feature(producer):
    assert 'Demo 1' == producer._get_feature().get('name')


def test___transform_feature(producer, transformed_feature):
    actual = producer._transform_feature()

    assert actual == transformed_feature


def test___transform_scenario(producer, scenario, transformed_scenario):
    actual = producer._transform_scenario(scenario)

    assert json.dumps(actual, sort_keys=True) == json.dumps(
        transformed_scenario, sort_keys=True)


def test__produce(producer, parsed_feature, transformed_output):
    actual = producer.transform()

    assert json.dumps(actual, sort_keys=True) == json.dumps(
        transformed_output, sort_keys=True)


def test___get_suite_location(producer, feature_file_name):
    assert producer._get_suite_location() == feature_file_name


def test__get_scenario_tags__inheritance(producer, scenario):
    assert '@feature_tag' in producer._get_tags(scenario)
