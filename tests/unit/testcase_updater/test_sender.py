"""Tests for Sender class"""

import pytest
from pytest_mock import MockFixture
from requests import exceptions as req_exc

from tantem_utils.sender import Sender
from tantem_utils.exceptions import SenderError, DataError
from tantem_utils.dtos import RequestPayload, DictRequestPayload
from tests.unit.utils import invalid_response, valid_response, valid_response_empty_payload


@pytest.fixture
def empty_request_payload() -> RequestPayload:
    return DictRequestPayload(data={})


@pytest.fixture
def request_payload() -> RequestPayload:
    return \
        DictRequestPayload(data={
            "projectId": "1234",
            "data": [
                {
                    "testSuiteName": "Login",
                    "suiteId": "Login",
                    "suiteLocation": "features/login.feature",
                    "tags": [
                        "@functional",
                        "@slow",
                        "@test_type=functional",
                        "@technology=LTE",
                        "@testbook_id=1234"
                    ],
                    "testCases": [
                        {
                            "testCaseName": "Login ok",
                            "testCaseId": "Login ok",
                            "tags": ["@functional", "@positive", "@slow"]
                        }
                    ]
                }
            ]
        })


@pytest.fixture
def sender(config, endpoint, request_payload, auth_data):
    s: Sender = Sender(endpoint=endpoint, config=config)
    s._data = request_payload
    return s


def test__send__invalid_payload_type__fail(sender):
    sender.payload = object
    with pytest.raises(DataError, match='Request'):
        sender.send()


def test__send__fail(mocker: MockFixture, sender: Sender):
    # arrange
    mocker.patch.object(sender._http, 'post',
                        side_effect=req_exc.RequestException)

    # act, assert
    with pytest.raises(SenderError):
        sender.send()


def test__send__invalid_response(
        mocker: MockFixture,
        sender: Sender,
        request_payload):
    # arrange
    mocker.patch.object(
        sender._authenticator,
        '_retrieve_token',
        return_value='token')
    mocker.patch.object(sender._http, 'post',
                        side_effect=invalid_response())

    # act, assert
    with pytest.raises(SenderError):
        sender.send(request_payload)


def test__send__200(mocker: MockFixture, sender: Sender, request_payload):
    # arrange
    mocker.patch.object(
        sender._authenticator,
        '_retrieve_token',
        return_value='token')
    mocker.patch.object(sender._http, 'put',
                        side_effect=valid_response())

    # act
    assert sender.send(request_payload)


def test__send_data_with_auth_header(mocker: MockFixture, sender: Sender):
    # arrange
    mocker.patch.object(
        sender._authenticator,
        '_retrieve_token',
        return_value='token')
    mocker.patch.object(sender._http, '_send', side_effect=valid_response())
    mocker.spy(sender._http, '_send')

    # act
    sender.send()

    # assert
    args = sender._http._send.call_args_list[0][1]
    headers = args.get('headers', {})
    assert 'Bearer token' in headers.get('Authorization', '')


def test__empty_response_payload(
        mocker: MockFixture,
        sender: Sender,
        empty_request_payload: DictRequestPayload):
    # arrange
    mocker.patch.object(
        sender._authenticator,
        '_retrieve_token',
        return_value='token')
    mocker.patch.object(
        sender._http,
        '_send',
        side_effect=valid_response_empty_payload())
    mocker.spy(sender._http, '_send')

    # act, assert
    assert sender.send()
    sender._http._send.assert_called_once()
