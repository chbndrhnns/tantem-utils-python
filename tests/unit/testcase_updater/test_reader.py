"""Tests for Reader class"""
import os
import pytest

from tantem_utils.testcase_updater.reader import Reader
from tantem_utils.exceptions import FeatureFileNotFoundError, ReaderError
from tests.unit import utils


@pytest.fixture
def multiple_mock_feature_files(tmpdir):
    dirs = ['', 'ui', 'backend', 'db']

    created = []
    for d in dirs:
        p = os.path.join(tmpdir, d)
        os.makedirs(p, exist_ok=True)
        file_name = utils.create_feature_file(p)
        created.append(file_name)

    return created


@pytest.fixture
def reader(path):
    return Reader(path=path)


@pytest.fixture
def feature_file(path, feature):
    file_name = os.path.join(path, 'dummy.feature')
    with open(file_name, 'w') as f:
        f.writelines(feature[1])
    yield file_name

    os.remove(file_name)


def test__file_exists__pass(path, feature_file):
    assert Reader(path).file_exists(feature_file)


def test__file_exists__no_file(path):
    assert False == Reader(path).file_exists({})


def test___path_exists__pass(path):
    assert Reader._path_exists(path)


def test___path_exists__fail():
    assert Reader._path_exists('dummy') == False


def test__get_files__pass(reader, path, multiple_mock_feature_files):
    assert len(reader.get_files()) == len(multiple_mock_feature_files)


def test__read_files__no_feature_files__exception(reader, path):
    with pytest.raises(ReaderError):
        reader.read_files()


def test___read_file__not_found(reader):
    with pytest.raises(FeatureFileNotFoundError):
        reader._read_file('bla.txt')


def test___read_file(reader, feature, feature_file):
    actual = reader._read_file(feature_file)
    assert actual == feature[1]


def test__read_files(reader, feature, feature_file):
    expected = {
        f'{feature_file}': feature[1]
    }

    actual = reader.read_files([feature_file])

    assert actual == expected
