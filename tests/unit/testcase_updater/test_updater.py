"""Tests for the updater"""
from typing import Callable

import pytest
import os

from pytest_mock import MockFixture

from tantem_utils import utils
from tantem_utils.config import Configuration
from tests.unit import utils as test_utils

from tantem_utils.testcase_updater.updater import TestcaseUpdater
from tantem_utils.exceptions import TestcaseUpdaterError
from tests.unit.utils import valid_response


@pytest.fixture
def multiple_mock_feature_files(tmpdir, feature):
    dirs = ['', 'ui', 'backend', 'db']

    created = []
    for d in dirs:
        p = os.path.join(tmpdir, d)
        os.makedirs(p, exist_ok=True)
        file_name = test_utils.create_feature_file(path=p, content=feature[1])
        created.append(file_name)

    return created


@pytest.fixture
def updater(config) -> TestcaseUpdater:
    return TestcaseUpdater(config=config)


def test__normalize_endpoint_url():
    non_normalized = ('http://localhost:4567/', '/abc')
    expected = 'http://localhost:4567/abc'

    assert expected == utils.normalize_endpoint(
        non_normalized[0], non_normalized[1])


def test__construct_endpoint_url(updater: TestcaseUpdater):
    # assert
    assert '1234' in updater.endpoint


def test__enrich_payload(transformed_scenarios, updater):
    res = updater.enrich_payload(transformed_scenarios)

    assert int(res.get('projectId'))
    assert isinstance(res.get('data'), list)


def test__auth_data(set_env_vars: Callable):
    # act
    config = Configuration()
    auth_data = config.auth_data

    # assert
    assert set(auth_data.keys()) == {'username', 'password'}
    assert all(val for val in auth_data.values())


def test__update__pass(
        updater,
        path,
        multiple_mock_feature_files,
        mocker):
    # arrange
    mocker.patch.object(
        updater._sender._authenticator,
        '_retrieve_token',
        return_value='token')
    mocker.patch.object(updater._sender._http, 'put',
                        side_effect=valid_response())

    # act, assert
    assert updater.run()


def test__update__no_feature_files(
        updater,
        path,
        multiple_mock_feature_files,
        mocker):
    # arrange
    mocker.patch('tantem_utils.testcase_updater.reader.Reader.get_files',
                 return_value=[])

    # act, assert
    with pytest.raises(TestcaseUpdaterError, match='feature'):
        updater.run()


def test__update__send_fails(
        updater,
        multiple_mock_feature_files):
    # act, assert
    with pytest.raises(TestcaseUpdaterError):
        updater.run()


def test__dry_run_does_not_call_send_method(
        mocker: MockFixture,
        updater: TestcaseUpdater,
        multiple_mock_feature_files):
    # arrange
    mocker.spy(updater, '_send_data')

    # act
    result: bool = updater.run(dry_run=True)

    # assert
    assert result
    updater._send_data.assert_not_called()
