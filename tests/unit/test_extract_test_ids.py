"""Tests for extractor.py"""
import configparser
import os

import pytest
from pytest_mock import MockFixture

from tantem_utils.extractor.__main__ import TestIdsExtractor


@pytest.fixture
def test_input_data() -> str:
    return 'Dummy Testcase 1,Dummy Testcase 2'


@pytest.fixture
def env(monkeypatch, test_input_data) -> None:
    monkeypatch.setenv(TestIdsExtractor.ENV_VAR, test_input_data)


@pytest.fixture
def extractor(env) -> TestIdsExtractor:
    return TestIdsExtractor()


def test__env_var(extractor: TestIdsExtractor):
    # assert
    assert extractor.env_var


def test__read_env_var(extractor: TestIdsExtractor, test_input_data: str):
    # assert
    assert extractor.raw_data == test_input_data


def test__config_is_ConfigParser(extractor: TestIdsExtractor):
    # assert
    assert isinstance(extractor.config_data, configparser.ConfigParser)


def test__config_has_behave_section(extractor: TestIdsExtractor):
    # assert
    section = extractor.config_data['behave']
    assert 'name' in section


def test__get_working_dir(extractor: TestIdsExtractor):
    #
    expected = os.getcwd()

    # act
    actual = extractor.working_dir

    # assert
    assert actual == expected


def test__get_other_working_dir(monkeypatch, mocker: MockFixture):
    # arrange
    monkeypatch.setenv(TestIdsExtractor.ENV_VAR, 'data')
    expected = '/var/abc/working/dir'
    mocked_getcwd = mocker.patch(
        'tantem_utils.extractor.__main__.os.getcwd',
        return_value=expected)

    # act
    actual = TestIdsExtractor().working_dir

    # assert
    assert actual == expected
    mocked_getcwd.assert_called_once()


def test_write_behave_config(mocker: MockFixture, extractor: TestIdsExtractor):
    # arrange
    mocked_write = mocker.patch(
        "builtins.open",
        mocker.mock_open())

    # act
    extractor.write_config()

    # assert
    mocked_write.assert_called_once()


def test__with_testids(extractor: TestIdsExtractor, test_input_data: str):
    # arrange
    scenario_names = test_input_data.split(",")
    scenario_names = ["^" + name + "$" for name in scenario_names]
    expected = "\n".join(scenario_names)

    # act
    actual = extractor.testcase_ids

    # assert
    assert expected == actual


def test__with_testids_strip_spaces(extractor: TestIdsExtractor):
    # arrange
    scenario_names = "Dummy 1, with spaces,     with many spaces"
    extractor._raw_data = scenario_names
    extractor._parse_and_store_input()

    scenario_names = [
        "^" +
        name.strip() +
        "$" for name in scenario_names.split(",")]
    expected = "\n".join(scenario_names)

    # act
    actual = extractor.testcase_ids

    # assert
    assert expected == actual
