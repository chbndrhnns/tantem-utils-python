"""Report uploader tests"""
import json
import pathlib

import pytest
from pytest_mock import MockFixture

from tantem_utils import exceptions as err, utils, ReportUploader
from tantem_utils.dtos import RequestPayload
from tantem_utils.exceptions import SenderError
from tantem_utils.tantem_dtos import ReportData
from tests.unit.conftest import mock_response
from tests.utils import _get_report_call_args


@pytest.fixture
def report_data() -> ReportData:
    return ReportData(pipeline_id=12345)


def test_create_instance(uploader: ReportUploader):
    # assert
    assert 'content' in uploader.endpoint


def test_create_payload_from_report(report_data, uploader: ReportUploader):
    # act
    payload = uploader._create_payload(report_data)

    # assert
    assert isinstance(payload, RequestPayload)


def test_upload_with_report_parameter_does_not_check_filesystem(
        mocker: MockFixture,
        uploader: ReportUploader,
        report_data):
    # arrange
    mocker.spy(uploader, 'compile_report_data')
    mocker.patch.object(uploader._sender, 'send', return_value=None)

    # act
    uploader.send_report(report_data)

    # assert
    assert not uploader.compile_report_data.called
    assert uploader._sender.send.assert_called_once


def test_upload_with_report_parameter(
        mocker: MockFixture,
        uploader: ReportUploader,
        report_data):
    # arrange
    mocker.spy(uploader._sender, 'send')
    mocker.patch.object(
        uploader._sender._http, '_send',
        side_effect=mock_response(content=''))

    # act
    uploader.send_report(report_data)

    # assert
    call_args = _get_report_call_args(uploader._sender.send.call_args)
    assert call_args.data.pipeline_id == report_data.pipeline_id
    uploader._sender.send.assert_called_once()


def test_upload_report(
        mocker: MockFixture,
        uploader: ReportUploader):
    # arrange
    mocker.spy(uploader._sender, 'send')
    mocker.patch.object(
        uploader._sender._http, '_send',
        side_effect=mock_response(content=''))

    # act
    uploader.send_report()

    # assert
    call_args = _get_report_call_args(uploader._sender.send.call_args)
    assert call_args.data.pipeline_id == uploader._config.pipeline_id
    uploader._sender.send.assert_called_once()


def test__validate_report_location(tmp_path,
                                   uploader: ReportUploader):
    # arrange
    tmp_file = tmp_path / 'report.json'
    tmp_file.write_text('{"a":"b"}')

    # act, assert
    assert uploader._is_valid_report_location(tmp_file)


def test_invalid_report_location_raises_ReportUploaderError(
        uploader: ReportUploader):
    # arrange
    uploader._config._config['REPORT_LOCATION'] = ''

    # assert
    with pytest.raises(err.ReportUploaderError, match='read'):
        uploader._read_report()


def test__validate_report_location__error(tmp_path,
                                          uploader: ReportUploader):
    # arrange
    tmp_file = tmp_path

    # act, assert
    assert not uploader._is_valid_report_location(tmp_file)


def test__read_empty_report_returns_string(uploader: ReportUploader):
    # act
    content = uploader._read_report()

    # assert
    assert isinstance(content, str)


def test__read_behave_report(mocker: MockFixture, uploader: ReportUploader):
    # arrange
    expected = """[
        {
          "keyword": "Feature",
          "location": "features/dummy.feature:2",
          "name": "Dummy Feature",
          "status": "skipped",
          "tags": [
            "ignore_tests"
          ]
        },

        {
          "keyword": "Feature",
          "location": "features/other_feature.feature:1",
          "name": "Dummy Feature 2",
          "status": "skipped",
          "tags": []
        },

        {
          "keyword": "Feature",
          "location": "features/other_features/third.feature:2",
          "name": "Dummy Feature 3",
          "status": "skipped",
          "tags": [
            "testbook_id=12"
          ]
        }
        ]"""
    mocker.patch(
        "builtins.open",
        mocker.mock_open(
            read_data=expected))

    # act
    actual = uploader._read_report()

    # assert
    assert isinstance(actual, str)
    assert json.loads(actual)


def test_upload_started(mocker: MockFixture, uploader: ReportUploader):
    # arrange
    mocker.spy(uploader, 'send_report')
    mocker.spy(utils, 'handle_silent_error')
    mocker.patch.object(uploader._sender, 'send', side_effect=SenderError())

    # act
    uploader.upload()

    #
    uploader.send_report.assert_called_once()
    utils.handle_silent_error.assert_called_once()


def test_compiled_report_data_can_parsed_as_json(
        uploader: ReportUploader,
        report_path: pathlib.PosixPath,
        example_report_content: str):
    # arrange
    expected = example_report_content
    report_path.write_text(expected)

    # act
    result: ReportData = uploader.compile_report_data()

    # assert
    assert isinstance(result, ReportData)
    assert json.loads(result.content)


def test_create_payload_creates_parsable_payload(
        uploader: ReportUploader,
        report_path: pathlib.PosixPath,
        example_report_content: str):
    # arrange
    data: ReportData = ReportData(
        pipeline_id=12345,
        content=example_report_content
    )

    # act
    actual: RequestPayload = uploader._create_payload(data)

    # assert
    assert isinstance(actual, RequestPayload)

    content: str = actual.to_json()
    assert json.loads(content)
