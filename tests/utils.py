"""Testing utils for unit, integration and e2e tests"""


def _get_report_call_args(call_args):
    return call_args[0][0]
