"""Integration tests with mocked connection"""
import json
import os
from typing import Optional

import pytest
from pytest_mock import MockFixture
from requests import auth as req_auth

from tests import ROOT_DIR

from tantem_utils import ReportUploader, Configuration
from tantem_utils.authenticator import Authenticator
from tantem_utils.dtos import RequestPayload, ResponsePayload, ReportPayload
from tantem_utils.http import HttpRequestAdapter


@pytest.fixture
def stub_http(endpoint) -> HttpRequestAdapter:
    class StubHttpRequestAdapter(HttpRequestAdapter):
        def __init__(self, endpoint: str):
            super().__init__(endpoint)

        def _send(self,
                  verb: str = 'get',
                  headers: Optional[dict] = None,
                  auth: Optional[req_auth.AuthBase] = None,
                  payload: Optional[RequestPayload] = None) -> ResponsePayload:
            return ResponsePayload()

    return StubHttpRequestAdapter(endpoint)


@pytest.mark.integration
def test_report_content(
        mocker: MockFixture,
        config: Configuration,
        stub_http: HttpRequestAdapter,
        stub_auth: Authenticator):
    # arrange
    config._config['REPORT_LOCATION'] = os.path.join(
        ROOT_DIR, 'integration', 'fixtures', 'results.json')

    uploader: ReportUploader = ReportUploader(config=config)
    uploader._sender._authenticator = stub_auth
    uploader._sender._http = stub_http
    mocker.spy(uploader._sender._http, '_send')

    # act
    uploader.send_report()

    # assert
    payload: ReportPayload = uploader._sender._http._send.call_args[1]['payload']
    assert payload
    assert json.loads(payload.data.content)
    uploader._sender._http._send.assert_called_once()
