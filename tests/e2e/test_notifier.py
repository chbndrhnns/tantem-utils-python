"""Test notifier"""
import logging
from typing import Dict

import pytest
from pytest_mock import MockFixture

from tantem_utils.config import Configuration
from tantem_utils.notifier.dtos import TestCaseStartedEvent, TestCaseStoppedEvent
from tantem_utils.tantem_dtos import EventData
from tantem_utils.notifier.notifier import Notifier

logger = logging.getLogger(__name__)  # pylint: disable=invalid-name


@pytest.mark.e2e
def test_notifier(
        mocker: MockFixture,
        integration_config: Configuration,
        execution_data: Dict,
        execution_pipeline):
    """This test sends dummy test case data to Tantem.

    In Tantem, a testing project exists with ID 3416 for that purpose.
    """
    # arrange
    notifier: Notifier = Notifier(integration_config)

    mocker.spy(notifier._sender, 'send')

    # act
    # TestCaseStartedEvent
    notifier.emit(event=TestCaseStartedEvent(data=EventData(
        pipeline_id=execution_pipeline.get('pipelineId'),
        test_case_id=execution_data.get('testCaseIds')[0],
        status='started',
        is_stopped=False
    )))
    # TestCaseStoppedEvent
    notifier.emit(event=TestCaseStoppedEvent(data=EventData(
        pipeline_id=execution_pipeline.get('pipelineId'),
        test_case_id=execution_data.get('testCaseIds')[0],
        status='started',
        is_stopped=True
    )))

    # assert
    assert notifier._sender.send.call_count == 2
