# Integration tests
import pytest
import os
import json

from tests import ROOT_DIR
from tantem_utils.testcase_updater.reader import Reader
from tantem_utils.testcase_updater.parser import GherkinParser
from tantem_utils.testcase_updater.producer import JsonProducer

testdata = [
    ('features/dummy.feature',
     {
         "suiteName": "Dummy Feature",
         "suiteId": "Dummy Feature",
         "suiteLocation": "features/dummy.feature",
         "tags": [],
         "testCases": [
             {
                 "testCaseName": "Dummy Scenario",
                 "testCaseId": "Dummy Scenario",
                 "tags": [

                 ]
             }
         ]
     }
     ),
    ('features/TC02_nmap.feature',
     {
         "suiteName": "TC02 nmap",
         "suiteId": "TC02 nmap",
         "suiteLocation": "features/TC02_nmap.feature",
         "tags": ["@TC02_nmap"],
         "testCases": [
             {
                 "testCaseName": "TC02 nmap",
                 "testCaseId": "TC02 nmap",
                 "tags": [
                     "@TC02_nmap"
                 ]
             }
         ]
     }
     )
]


@pytest.fixture
def reader(path):
    return Reader(path)


@pytest.fixture
def producer():
    return JsonProducer


@pytest.fixture
def parser():
    return GherkinParser()


def get_absolute_path(filename):
    return os.path.realpath(os.path.join(ROOT_DIR, '..', filename))


@pytest.mark.parametrize('in_file, expected', testdata)
@pytest.mark.e2e
def test__transformation__no_upload(
        reader,
        producer,
        parser,
        in_file,
        expected):
    with open(in_file, 'r') as f:
        content = reader._read_file(get_absolute_path(in_file))

    parsed_content = parser.parse(content)
    actual = producer(parsed_content, in_file).transform()

    assert json.dumps(actual, sort_keys=True) == json.dumps(
        expected, sort_keys=True)
