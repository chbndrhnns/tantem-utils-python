"""Test updater"""
import logging

import pytest
import requests
from requests import Response

from tantem_utils.config import Configuration
from tantem_utils.testcase_updater.updater import TestcaseUpdater
from tests.e2e.utils import _get_sent_case_ids, _get_received_ids

logger = logging.getLogger(__name__)  # pylint: disable=invalid-name


@pytest.mark.e2e
def test_updater(updater: TestcaseUpdater,
                 integration_config: Configuration,
                 headers: dict,
                 tantem_project_id: int,
                 ensure_no_test_is_running):
    """This test sends dummy test case data to Tantem.

    In Tantem, a testing project exists with ID 3416 for that purpose.
    """
    # act
    result = updater.run()
    sent_case_ids: set = _get_sent_case_ids(updater._data)

    # get data back from Tantem
    assert result

    endpoint = f'{integration_config.base_url}/tim/testautomationmicrot/api/project/{tantem_project_id}/test/case/all'
    res: Response = requests.get(url=endpoint,
                                 headers=headers,
                                 verify=integration_config.verify_ssl)
    received_case_ids: set = _get_received_ids(res)

    assert not sent_case_ids.difference(received_case_ids)
