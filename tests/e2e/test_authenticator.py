"""Integration tests for authenticator"""

import pytest

from tantem_utils.config import Configuration
from tantem_utils.authenticator import Authenticator


@pytest.mark.e2e
def test_authenticator(integration_config: Configuration):
    """This test tries to authenticate with Tantem.

    In Tantem, a testing project exists with ID 3416 for that purpose.
    """
    # arrange
    a = Authenticator(integration_config)

    # act
    token = a.token

    # assert
    assert len(token) > 0
    assert isinstance(token, str)
