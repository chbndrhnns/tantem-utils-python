import json
import logging

import requests
from requests import Response

logger = logging.getLogger(__name__)  # pylint: disable=invalid-name


def _get_sent_case_ids(data) -> set:
    """Helper method to get a set of test cases from the updater"""
    ids = []
    for suite in data:
        for case in suite.get('testCases', []):
            ids.append(case.get('testCaseId'))
    return set(ids)


def _get_received_ids(data) -> set:
    """Helper method to get a set of test cases from a response"""
    return set([testcase.get('testId') for testcase in data.json()])


def stop_execution(execution_id, pipeline_id, integration_config, headers):
    """Stop the test execution in Tantem"""
    endpoint: str = f'{integration_config.base_url}/tim/testautomationmicrot/api/report/content'
    logger.info(
        f'Stopping execution {execution_id} in pipeline {pipeline_id}...')
    data = {
        'pipelineId': pipeline_id,
        'content': 'e2e test'
    }
    res: Response = requests.put(
        url=endpoint,
        headers=headers,
        data=json.dumps(data),
        verify=integration_config.verify_ssl)
    res.raise_for_status()
    logger.info(
        f'Execution {execution_id} in pipeline {pipeline_id} successfully stopped...')
