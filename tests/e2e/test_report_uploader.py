"""Integration test for report uploader"""
import pytest
from pytest_mock import MockFixture

from tantem_utils.config import Configuration
from tantem_utils import ReportUploader
from tantem_utils.tantem_dtos import ReportData
from tests.utils import _get_report_call_args


@pytest.mark.e2e
def test_report_uploader(
        mocker: MockFixture,
        execution_pipeline,
        integration_config: Configuration):
    """
    This test sends a test report to Tantem.
    It also implicitly stops test execution.
    """

    # arrange
    report_data: ReportData = ReportData(
        pipeline_id=execution_pipeline.get('pipelineId'),
        content='{"content": "This test was just finished by an automated e2e test."}')

    uploader: ReportUploader = ReportUploader(config=integration_config)
    mocker.spy(uploader._sender, 'send')

    # act
    uploader.send_report(report_data)

    # assert
    uploader._sender.send.assert_called_once()
    report_call_arg = _get_report_call_args(uploader._sender.send.call_args)
    assert report_call_arg.data == report_data
