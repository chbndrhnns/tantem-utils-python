import json
import os
from typing import Dict, List

import pytest
import requests
from requests import Response

from tests import ROOT_DIR
from tantem_utils.authenticator import Authenticator
from tantem_utils.config import Configuration
from tantem_utils.notifier.dtos import TestCaseStartedEvent
from tantem_utils.tantem_dtos import EventData
from tantem_utils.testcase_updater.updater import TestcaseUpdater
from tests.e2e.test_notifier import logger
from tests.e2e.test_updater import logger
from tests.e2e.utils import stop_execution, _get_sent_case_ids


@pytest.fixture(scope="session")
def monkeysession(request):
    """source: https://github.com/pytest-dev/pytest/issues/363#issuecomment-406536200"""
    from _pytest.monkeypatch import MonkeyPatch
    mpatch = MonkeyPatch()
    yield mpatch
    mpatch.undo()


@pytest.fixture(scope='module')
def integration_config(monkeysession) -> Configuration:
    monkeysession.setenv(
        'CI_PROJECT_URL',
        'https://gitlab.tools.in.pan-net.eu/TAAS/testing/tantem-integration-tests')
    config = {
        'CI_PROJECT_ID': 3416,
        'CI_PIPELINE_ID': 54321,
        'SSL_VERIFY': 'True',
        'TANTEM_AUTH_USER': 'gitlab-user@pan-net.eu',
        'TANTEM_AUTH_PASSWORD': os.getenv(
            'TANTEM_AUTH_PASSWORD',
            '').strip(),
        'TANTEM_BASE_URL': 'https://tantem-dev.ctf.in.pan-net.eu',
        'TESTS_LOCATION': os.path.realpath(
            os.path.join(
                ROOT_DIR,
                '..',
                'features'))}
    c: Configuration = Configuration(from_dict=config)
    return c


@pytest.fixture(scope='module')
def authenticator(integration_config: Configuration):
    return Authenticator(integration_config)


@pytest.fixture
def event_started_data(integration_config: Configuration):
    return EventData(pipeline_id=integration_config.project_id,
                     test_case_id='Dummy Scenario 1',
                     status='Passed'
                     )


@pytest.fixture
def event_started(event_started_data: EventData):
    return TestCaseStartedEvent(data=event_started_data)


@pytest.fixture(scope='module')
def auth_header(integration_config: Configuration,
                authenticator: Authenticator) -> dict:
    return {
        'Authorization': f'Bearer {authenticator.token}'
    }


@pytest.fixture(scope='module')
def headers(auth_header: dict) -> dict:
    auth_header.update({
        'content-type': 'application/json'
    })
    return auth_header


@pytest.fixture(scope='module')
def updater(integration_config: Configuration):
    return TestcaseUpdater(integration_config)


@pytest.fixture
def ensure_no_test_is_running(
        headers: dict,
        integration_config: Configuration,
        tantem_project_id: int):
    """
    We need to ensure that no test execution is currently running.
    This can be done by sending a report.
    """
    # get executions
    logger.info(
        f'Getting executions for tantem project {tantem_project_id}...')
    endpoint: str = f'{integration_config.base_url}/tim/testautomationmicrot/api/project/{tantem_project_id}/test/report/all'
    res: Response = requests.get(
        url=endpoint,
        headers=headers,
        verify=integration_config.verify_ssl)
    res.raise_for_status()

    # stop running executions
    running_executions = {ex.get('id'): ex.get('pipelineId')
                          for ex in res.json() if not ex.get('isStopped')}
    for execution_id, pipeline_id in running_executions.items():
        stop_execution(execution_id, pipeline_id, integration_config, headers)


@pytest.fixture
def tantem_project_id(integration_config: Configuration, headers: dict) -> int:
    """Get tantem_project_id for testing project"""
    logger.info('Getting all projects to identify test project...')
    endpoint: str = f'{integration_config.base_url}/tim/testautomationmicrot/api/project/all'
    res: Response = requests.get(
        url=endpoint,
        headers=headers,
        verify=integration_config.verify_ssl)
    res.raise_for_status()
    tantem_project_id: int = [project.get('id') for project in res.json(
    ) if project.get('gitlabProjectId') == f'{integration_config.project_id}'][0]
    return tantem_project_id


@pytest.fixture(scope='module', autouse=True)
def execution_pipeline(request,
                       execution_data: dict,
                       headers: dict,
                       integration_config: Configuration) -> Dict:
    """Create test execution before running the test"""
    logger.info('Create execution')

    endpoint: str = f'{integration_config.base_url}/tim/testautomationmicrot/api/test/execute'
    res: Response = requests.post(
        url=endpoint,
        headers=headers,
        data=json.dumps(execution_data),
        verify=integration_config.verify_ssl)
    logger.debug('Response: %s', res.content)
    res.raise_for_status()

    data = res.json()

    pipeline_id: int = data.get('pipelineId')
    logger.info(
        f'Execution pipeline {pipeline_id} created with test cases: {execution_data.get("testCaseIds")}')

    yield data

    def teardown():
        logger.info('test teardown')
        stop_execution(
            data.get('id'),
            pipeline_id,
            integration_config,
            headers)

    request.addfinalizer(teardown)


@pytest.fixture(scope='module')
def execution_data(
        integration_config: Configuration,
        updater_data: Dict) -> Dict:
    testcases: List = list(_get_sent_case_ids(updater_data))
    return {
        "gitlabProjectId": integration_config.project_id,
        "testCaseIds": testcases}


@pytest.fixture(scope='module')
def updater_data(updater: TestcaseUpdater, integration_config: Configuration):
    updater.run(dry_run=True)
    return updater._data
