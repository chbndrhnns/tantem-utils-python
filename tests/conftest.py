"""conftest.py for all tests"""
import pytest

from tantem_utils import Configuration, TestcaseUpdater, ReportUploader
from tantem_utils.authenticator import Authenticator

from tantem_utils.dtos import RequestPayload, EventDataRequestPayload
from tantem_utils.notifier.dtos import TestCaseStartedEvent, TestCaseStoppedEvent
from tantem_utils.sender import Sender
from tantem_utils.tantem_dtos import EventData


@pytest.fixture
def path(tmpdir):
    return tmpdir


@pytest.fixture
def event_started(event_started_data: EventData):
    return TestCaseStartedEvent(data=event_started_data)


@pytest.fixture
def event_started_data():
    return EventData(pipeline_id=123, test_case_id='123', status='running')


@pytest.fixture
def event_stopped_data():
    return EventData(pipeline_id=123, test_case_id='123', status='stopped')


@pytest.fixture
def event_stopped(event_stopped_data: EventData):
    return TestCaseStoppedEvent(data=event_stopped_data)


@pytest.fixture
def event_data_request_payload(event_started_data) -> RequestPayload:
    return EventDataRequestPayload(data=event_started_data)


@pytest.fixture
def stub_auth(config: Configuration) -> Authenticator:
    class StubAuth(Authenticator):
        def __init__(self):
            super().__init__(config=config)

        def _retrieve_token(self) -> str:
            return 'token'

    return StubAuth()


@pytest.fixture
def stub_sender(config: Configuration) -> Sender:
    class StubSender(Sender):
        def __init__(self):
            super().__init__(
                endpoint='bla',
                config=config)

        def send(self, data: RequestPayload = None) -> True:
            return True

    return StubSender()


@pytest.fixture
def config(set_env_vars, monkeypatch, report):
    monkeypatch.setenv('REPORT_LOCATION', str(report))
    config: Configuration = Configuration()
    return config


@pytest.fixture
def set_env_vars(monkeypatch, path):
    monkeypatch.setenv('CI_PROJECT_ID', '1234')
    monkeypatch.setenv('CI_PIPELINE_ID', '54321')
    monkeypatch.setenv('TANTEM_BASE_URL', 'https://tantem.tools.in.pan-net.eu')
    monkeypatch.setenv('TANTEM_AUTH_PASSWORD', 'noRealPassword')
    monkeypatch.setenv('TANTEM_AUTH_USER', 'tantem-user')
    monkeypatch.setenv('TESTS_LOCATION', str(path))
    monkeypatch.setenv('SSL_VERIFY', 'True')


@pytest.fixture
def report_path(tmp_path):
    return tmp_path / 'report.json'


@pytest.fixture
def report(report_path) -> None:
    report_path.write_text('')
    return report_path


@pytest.fixture
def example_report_content() -> str:
    return """[
        {
            "elements": [
                {
                    "keyword": "Scenario",
                    "location": "features/dummy.feature:4",
                    "name": "Dummy Scenario 1",
                    "status": "passed",
                    "steps": [
                        {
                            "keyword": "Given",
                            "location": "features/dummy.feature:6",
                            "match": {
                                "arguments": [],
                                "location": "features/steps/step_dummy.py:16"
                            },
                            "name": "Nothing",
                            "result": {
                                "duration": 0.0005488395690917969,
                                "status": "passed"
                            },
                            "step_type": "given"
                        }
                    ],
                    "tags": [],
                    "type": "scenario"
                }
            ],
            "keyword": "Feature",
            "location": "features/dummy.feature:2",
            "name": "Dummy Feature",
            "status": "passed",
            "tags": [
                "ignore_tests"
            ]
        }
    ]"""


@pytest.fixture
def endpoint():
    return f'http://localhost:1234{TestcaseUpdater.ENDPOINT}'


@pytest.fixture
def uploader(
        config: Configuration,
        stub_auth: Authenticator) -> ReportUploader:
    uploader: ReportUploader = ReportUploader(config=config)
    uploader._sender._authenticator = stub_auth

    return uploader
