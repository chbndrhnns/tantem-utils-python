IMAGE:=tantem-testcase-updater

# Variables taken from the environment with defaults if not set there
TESTS_LOCATION?=
TANTEM_BASE_URL?=https://tantem-dev.ctf.in.pan-net.eu
CI_PROJECT_ID?=3416
LOG_LEVEL?=INFO

# Build python package
.PHONY: sdist
sdist:
	@pipenv run python setup.py sdist

# Build the Docker image
.PHONY: build
build:
	@docker build -t  ${IMAGE} .

# Run the Docker image
.PHONY: run
run:
	@docker run \
		-e "CI_PROJECT_ID=${CI_PROJECT_ID}" \
		-e "TANTEM_BASE_URL=${TANTEM_BASE_URL}" \
		-e "TESTS_LOCATION=/app/features" \
		-e "LOG_LEVEL=${LOG_LEVEL}" \
		-v ${TESTS_LOCATION}:/app/features \
	-it ${IMAGE} \
	pipenv run python main.py
