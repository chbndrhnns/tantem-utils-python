FROM python:3.7-alpine

LABEL maintainer="johannes.rueschel@telekom.de"

WORKDIR /app

# Add Pan-Net Root Certificate(s)
RUN apk add --no-cache --update ca-certificates && \
  wget http://info.pki.sec.in.pan-net.eu/crts/rootcax1.crt -P /usr/local/share/ca-certificates/ && \
  update-ca-certificates

ENV REQUESTS_CA_BUNDLE=/etc/ssl/certs/ca-certificates.crt

RUN pip install pipenv

COPY Pipfile .
COPY Pipfile.lock .

RUN pipenv install

COPY main.py .
COPY tantem_utils tantem_utils
