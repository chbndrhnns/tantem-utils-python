"""setup.py"""
from setuptools import setup, find_packages
from os import path
from tantem_utils import __version__

# read README.md file
current_dir = path.abspath(path.dirname(__file__))
with open(path.join(current_dir, 'README.md'), encoding='utf-8') as inp:
    long_description = inp.read()

setup(
    name='tantem-utils',
    url='https://gitlab.tools.in.pan-net.eu/TAAS/tantem-utils-python/',
    author='Johannes Rüschel',
    author_email='johannes.rueschel@telekom.de',
    packages=find_packages(),
    install_requires=[
        'gherkin-official',
        'requests'
    ],
    version=__version__,
    description='Utilities to communicate with Tantem',
    long_description=long_description
)
